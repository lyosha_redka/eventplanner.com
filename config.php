<?php
// HTTP
define('HTTP_SERVER', 'http://event-planner.local/');

// HTTPS
define('HTTPS_SERVER', 'http://event-planner.local/');

// DIR
define('DIR_APPLICATION', 'C:/wamp64/www/event-planner.local/catalog/');
define('DIR_SYSTEM', 'C:/wamp64/www/event-planner.local/system/');
define('DIR_IMAGE', 'C:/wamp64/www/event-planner.local/image/');
define('DIR_LANGUAGE', 'C:/wamp64/www/event-planner.local/catalog/language/');
define('DIR_TEMPLATE', 'C:/wamp64/www/event-planner.local/catalog/view/theme/');
define('DIR_CONFIG', 'C:/wamp64/www/event-planner.local/system/config/');
define('DIR_CACHE', 'C:/wamp64/www/event-planner.local/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/wamp64/www/event-planner.local/system/storage/download/');
define('DIR_LOGS', 'C:/wamp64/www/event-planner.local/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/wamp64/www/event-planner.local/system/storage/modification/');
define('DIR_UPLOAD', 'C:/wamp64/www/event-planner.local/system/storage/upload/');

define('DIR_BASE', 'C:\wamp64\www\event-planner.local/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'user_event');
define('DB_PASSWORD', 'asuspasus888');
define('DB_DATABASE', 'event');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
