<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="/image/catalog/2.png" rel="icon" />

<title><?php echo $title; ?></title>

<?php if ($noindex) { ?>
<!-- OCFilter Start -->
<meta name="robots" content="noindex,nofollow" />
<!-- OCFilter End -->
<?php } ?>
      
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/materialize.css">

<style>
@import url('https://fonts.googleapis.com/css?family=Montserrat&display=swap');
</style>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/3.1.1/jquery.js"></script>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
<script src="/catalog/view/javascript/social_auth.js" type="text/javascript"></script>



<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v5.0"></script>


<!-- <link href="/catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" rel="stylesheet">
<script src="/catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script> -->


<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js" type="text/javascript"></script>


<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">


<div class="main-header">

  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>

      <div class="col-sm-8">

        <nav id="top">
            
            <div id="top-links" class="nav pull-right">
              <ul class="list-inline visible-sm visible-md visible-lg">

                <li class="block-my-planner"><?php if ($logged) { ?> <a href="#" class="my-planner-btn"><img src="/image/star.png" class="star-icon">Мой планировщик</a> <?php } else { ?> <a href="<?php echo $login; ?>" class="my-planner-btn"><img src="/image/star.png" class="star-icon">Мой планировщик</a> <?php } ?></li>


                <!-- <li class="btn-login-block"><a href="<?php echo $login; ?>" class="login-btn-header">Войти</a> | <a href="<?php echo $register; ?>" class="login-btn-header">Регистрация</a></li> -->

                <li class="btn-login-block">
                <?php if ($logged) { ?>
                  <li class="dropdown">
                    <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $FirstName; ?></span> <span class="caret"></span></a>

                    <ul class="dropdown-menu dropdown-menu-right">
                      <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                      <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                    </ul>

                  </li>
                <?php } else { ?>
                  <a class="login-btn-header" id="login-popup">Войти</a> | <a class="login-btn-header popupUp-register">Регистрация</a>
                <?php } ?>
                </li>

                <?php if ($logged) { ?>

                <?php } else { ?>
                  <li class="btn-login-block"><a href="<?php echo $register; ?>" class="portfolio-btn-header">Разместить портфолио</a></li>
                <?php } ?>

                <li class="lang-list"><?php echo $language; ?></li>

                <div class="overlay"></div>
                <div id="form-step-first" class="moveRight popupWindow">
                  <div class="popup-close">+</div>
                  <div class="popup-form-choice">
                    <p class="popup-title-choice">Регистрируясь на портале вы - </p>
                    <div class="link-choice">
                      <a href="<?php echo $register; ?>" class="link-href-register search-uslug-link-register">Ищите услугу <br />или локацию</a>
                      <a href="<?php echo '#' ?>" class="link-href-register proposal-link-register">Оказываете <br /> услугу</a>
                    </div>
                  </div>
                </div>
                
                <!-- <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a> -->
                  <!-- <ul class="dropdown-menu dropdown-menu-right">
                    <?php if ($logged) { ?>
                    <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                    <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                    <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                    <?php } ?>
                  </ul>
                </li> -->
              </ul>
              <div class="lang-list"></div>
            </div>
        </nav>

        <div class="container-menu">
          
          
			<?php
			if($use_megamenu)
			{?>
			<script type="text/javascript" src="/catalog/view/javascript/megamenu/megamenu.js?v3"></script>
<link rel="stylesheet" href="/catalog/view/theme/default/stylesheet/megamenu.css?v3">
	<div class="header-bg">
<div class="container2">
  <nav id="megamenu-menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs-none"><?php echo $menu_title; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle btn-nav-menu" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($items as $item) { ?>
        <?php if ($item['children']) { ?>
		
        <li class="dropdown">
		
		<a href="<?php echo $item['href']; ?>" <?php if($item['use_target_blank'] == 1) { echo ' target="_blank" ';} ?> <?php if($item['type'] == "link") {echo 'data-target="link"';} else {echo 'class="dropdown-toggle dropdown-img" data-toggle="dropdown"';} ?>><?php if($item['thumb']){?>
		<img class="megamenu-thumb" src="<?=$item['thumb']?>"/>
		<?php } ?><?php echo $item['name']; ?></a>
		
		<?php if($item['type']=="category"){?>
		<?php if($item['subtype']=="simple"){?>
          <div class="dropdown-menu megamenu-type-category-simple">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
            
					  
			    <ul class="list-unstyled megamenu-haschild">
                <?php foreach ($children as $child) { ?>
                <li class="<?php if(count($child['children'])){?> megamenu-issubchild<?php } ?>"><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				
				<?php if(count($child['children'])){?>
				<ul class="list-unstyled megamenu-ischild megamenu-ischild-simple">
				 <?php foreach ($child['children'] as $subchild) { ?>
				<li><a href="<?php echo $subchild['href']; ?>"><?php echo $subchild['name']; ?></a></li>				
				<?php } ?>
				</ul>
				<?php } ?>				
				</li>
                <?php } ?>
				</ul>
				
				
				
              <?php } ?>
            </div>            
			</div>
			<?php } ?>	
			<?php } ?>
			
		<?php if($item['type']=="category"){?>
		<?php if($item['subtype']=="full"){?>
          <div class="dropdown-menu megamenu-type-category-full megamenu-bigblock">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
            
			  	<?php if($item['add_html']){?>
			  <div style="" class="menu-add-html">
				<?=$item['add_html'];?>
				</div>
				<?php } ?>
			  
			    <ul class="list-unstyled megamenu-haschild">
                <?php foreach ($children as $child) { ?>
                <li class="megamenu-parent-block<?php if(count($child['children'])){?> megamenu-issubchild<?php } ?>"><a class="megamenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				
				<?php if(count($child['children'])){?>
				<ul class="list-unstyled megamenu-ischild">
				 <?php foreach ($child['children'] as $subchild) { ?>
				<li><a href="<?php echo $subchild['href']; ?>"><?php echo $subchild['name']; ?></a></li>				
				<?php } ?>
				</ul>
				<?php } ?>				
				</li>
                <?php } ?>
				</ul>
              <?php } ?>
            </div>            
			</div>
			<?php } ?>	
			<?php } ?>
			
			<?php if($item['type']=="category"){?>
		<?php if($item['subtype']=="full_image"){?>
          <div class="dropdown-menu megamenu-type-category-full-image megamenu-bigblock">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
            
			  	<?php if($item['add_html']){?>
			  <div style="" class="menu-add-html">
				<?=$item['add_html'];?>
				</div>
				<?php } ?>
			  
			    <ul class="list-unstyled megamenu-haschild">
                <?php foreach ($children as $child) { ?>
                <li class="megamenu-parent-block<?php if(count($child['children'])){?> megamenu-issubchild<?php } ?>">
				<a class="megamenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="" title=""/></a>
				<a class="megamenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				
				<?php if(count($child['children'])){?>
				<ul class="list-unstyled megamenu-ischild">
				 <?php foreach ($child['children'] as $subchild) { ?>
				<li><a href="<?php echo $subchild['href']; ?>"><?php echo $subchild['name']; ?></a></li>				
				<?php } ?>
				</ul>
				<?php } ?>				
				</li>
                <?php } ?>
				</ul>
              <?php } ?>
            </div>            
			</div>
			<?php } ?>	
			<?php } ?>
			
			
			<?php if($item['type']=="html"){?>
		
          <div class="dropdown-menu megamenu-type-html">
            <div class="dropdown-inner">
              
            
			  
			  
			    <ul class="list-unstyled megamenu-haschild">
                
                <li class="megamenu-parent-block<?php if(count($child['children'])){?> megamenu-issubchild<?php } ?>">
				<div class="megamenu-html-block">				
				<?=$item['html']?>
				</div>
				</li>
              				</ul>
            
            </div>            
			</div>
				
			<?php } ?>
			
			
		
			
		<?php if($item['type']=="manufacturer"){?>

          <div class="dropdown-menu megamenu-type-manufacturer <?php if($item['add_html']){?>megamenu-bigblock<?php } ?>">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
            
			   <?php if($item['add_html']){?>
			  <div style="" class="menu-add-html">
				<?=$item['add_html'];?>
				</div>
				<?php } ?>
			  
			    <ul class="list-unstyled megamenu-haschild <?php if($item['add_html']){?>megamenu-blockwithimage<?php } ?>">
                <?php foreach ($children as $child) { ?>
                <li class="megamenu-parent-block">
				<a class="megamenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="" title="" /></a>
				<a class="megamenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				
					
				</li>
                <?php } ?>
				</ul>
              <?php } ?>
            </div>            
			</div>
		
			<?php } ?>
			
				<?php if($item['type']=="information"){?>
	
          <div class="dropdown-menu megamenu-type-information <?php if($item['add_html']){?>megamenu-bigblock<?php } ?>">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
            
			  <?php if($item['add_html']){?>
			  <div style="" class="menu-add-html">
				<?=$item['add_html'];?>
				</div>
				<?php } ?>
			  
			    <ul class="list-unstyled megamenu-haschild <?php if($item['add_html']){?>megamenu-blockwithimage<?php } ?>">
                <?php foreach ($children as $child) { ?>
                <li class=""><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				
					
				</li>
                <?php } ?>
				</ul>
              <?php } ?>
            </div>            
			</div>
		
			<?php } ?>
			
			
			
				<?php if($item['type']=="product"){?>

          <div class="dropdown-menu megamenu-type-product <?php if($item['add_html']){?>megamenu-bigblock<?php } ?>">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
            
			   <?php if($item['add_html']){?>
			  <div style="" class="menu-add-html">
				<?=$item['add_html'];?>
				</div>
				<?php } ?>
			  
			    <ul class="list-unstyled megamenu-haschild <?php if($item['add_html']){?>megamenu-blockwithimage<?php } ?>">
                <?php foreach ($children as $child) { ?>
                <li class="megamenu-parent-block">
				<a class="megamenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="" title="" /></a>
				<a class="megamenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				<div class="dropprice">
				<?php if($child['special']){?><span><?php } ?><?php echo $child['price']; ?><?php if($child['special']){?></span><?php } ?><?php echo $child['special']; ?>
				</div>				
				</li>
                <?php } ?>
				</ul>
              <?php } ?>
            </div>            
			</div>
		
			<?php } ?>
			
			
					<?php if($item['type']=="auth"){?>
		
          <div class="dropdown-menu megamenu-type-auth">
            <div class="dropdown-inner">
              
            
			  
			  
			    <ul class="list-unstyled megamenu-haschild">
                
                <li class="megamenu-parent-block<?php if(count($child['children'])){?> megamenu-issubchild<?php } ?>">
				<div class="megamenu-html-block">				
				  
				  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
				<a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></div>
				
              <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />
             
            </form>
			
			
				</div>
				</li>
              				</ul>
            
            </div>            
			</div>
				
			<?php } ?>
			
        </li>

	   
		
		
		
         <?php } ?>
        <?php if (!$item['children']) { ?>
        <li><a href="<?php echo $item['href']; ?>"><?php echo $item['name']; ?></a></li>
        <?php } ?>
        <?php } ?>		

<li class="poisk-custom"><?php echo $search ?></li>
      </ul>
    </div>
  </nav>
</div>
</div>
<?php } ?>
<?php if ($categories && !$use_megamenu) { ?>

			
          <div class="container2">
            <nav id="menu" class="navbar">
              <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
                <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
              </div>
              <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                  <?php foreach ($categories as $category) { ?>
                  <?php if ($category['children']) { ?>
                  <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
                    <div class="dropdown-menu">
                      <div class="dropdown-inner">
                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                        <ul class="list-unstyled">
                          <?php foreach ($children as $child) { ?>
                          <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                          <?php } ?>
                        </ul>
                        <?php } ?>
                      </div>
                      <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
                  </li>
                  <?php } else { ?>
                  <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                  <?php } ?>
                  <?php } ?>
                </ul>
              </div>

            </nav>
          </div>
          <?php } ?>

        </div>

        
      </div>
    </div>
  </div>
  </div>

<div class="for-fixed-header">






<!-- <header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        
      </div>
     <div class="col-sm-5"><?php // echo $search; ?>
      </div>
    </div>
  </div>
</header> -->

