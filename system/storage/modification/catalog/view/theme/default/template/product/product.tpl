<?php echo $header; ?>
<div class="container">


  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>


  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row margin-vendors-border">

        <div class="col-sm-12">
          <div class="bg-image-vendor" style="background-image: url('<?php echo $thumb; ?>')">
            <!-- <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /> -->

            <div class="flex-row-head">
              <div class="flex-head-vendors">
                <div class="title-block-vendors">
                  <h1 class="title-vendors"><?php if ($category_id == '77') { echo 'Ресторан';} else {} ?><span> <?php echo $heading_title; ?></span></h1>
                </div>

                <div class="rating-gallery">

                  <?php if ($review_status) { ?>
                    <div class="rating">
                      <p>
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($rating < $i) { ?>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                        <?php } else { ?>
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                        <?php } ?>
                        <?php } ?>
                        <a class="link-otzyv-vendors scroll" href="#nav-link" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a></p>
                    </div>
                  <?php } ?>

                  <?php if ($thumb || $images) { ?>
                    <div class="gallery-head">
                      <?php $i = 0; ?>
                      <?php foreach ($images as $image) { ?>
                        <?php $i = $i + 1; ?>
                      <?php } ?>
                      <a class="link-otzyv-vendors scroll" href="#nav-link" onclick="$('a[href=\'#gallery-vendors\']').trigger('click'); return false;"><?php echo '<img src="/image/catalog/gallery.png" class="gallery-img-icon"> <span class="gallery-text-head">Галерея (<span class="medium-helvet">' . $i . '</span><span> фото</span>)</span>'; ?></a>
                    </div>
                  <?php } ?>
                </div>
              </div>

              <div class="flex-btn-head-vendors">
                <button type="button" data-toggle="tooltip" class="btn btn-default wish-btn-vendors" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><span class="add-izbr">добавить в избранное</span>
                <img src="/image/catalog/icon-svg/heart_white.svg" class="like-img-header like-img-cat"></button>
              </div>
            </div>

            
            
            

          </div>
        </div>

        <div class="col-sm-6">


          <!--Картинки изображения START-->

          

          <!--Картинки изображения END-->

          

          <?php if ($attribute_groups) { ?>

            <div class="tab-pane active" id="tab-specification">
              <div class="block-attribute">
                  <div class="row-price">
                      <div class="price-vendors"><span class="attr-name-vendors">Средний чек</span></div>
                      <div class="value-attribute-vendors"><?php echo $location . ' - ';  ?><?php echo $price ?></div>
                    </div>
                  <?php foreach ($attribute_groups as $attribute_group) { ?>
                    
                  <div class="row-attribute">
                    <!-- ЦИКЛ ПО АТРИБУТАМ ВЕНДОРА START -->
                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>

                    <?php if($attribute['name'] == 'Кухня') { ?>
                    <div class="block-vendors-attribute kuhya-class">

                        <!-- КАТЕГОРИЯ ЗАВЕДЕНИЯ START -->
                        <div class="name-attribute-vendors">
                          <span class="attr-name-vendors"><?php echo $attribute['name']; ?></span>
                        </div>
                        <div class="value-attribute-vendors left-dop-mar"> <?php echo $attribute['text']; ?></div>
                        <!-- КАТЕГОРИЯ ЗАВЕДЕНИЯ END -->

                    </div>
                  <?php } else if ($attribute['name'] == 'Особенности') { ?>
                      <div class="block-vendors-attribute kuhya-class">

                        <!-- КАТЕГОРИЯ ЗАВЕДЕНИЯ START -->
                        <div class="name-attribute-vendors">
                          <span class="attr-name-vendors"><?php echo $attribute['name']; ?></span>
                        </div>
                        <div class="value-attribute-vendors left-dop-mar"> <?php echo $attribute['text']; ?></div>
                        <!-- КАТЕГОРИЯ ЗАВЕДЕНИЯ END -->

                    </div>
                    <?php } else { ?>
                      <div class="block-vendors-attribute">

                        <!-- КАТЕГОРИЯ ЗАВЕДЕНИЯ START -->
                        <div class="name-attribute-vendors">
                          <span class="attr-name-vendors"><?php echo $attribute['name']; ?></span>
                        </div>
                        <div class="value-attribute-vendors left-dop-mar"> <?php echo $attribute['text']; ?></div>
                        <!-- КАТЕГОРИЯ ЗАВЕДЕНИЯ END -->
                    
        

                    </div>
                  <?php } ?>
                    <?php } ?>
                    <!-- ЦИКЛ ПО АТРИБУТАМ ВЕНДОРА END -->
                  </div>
                  <?php } ?>

              </div>
            </div>

          <?php } ?>

        
        <div class="anons-block-vendors">
          <?php echo $anons; ?>

          <div class="share-vendors-blocks">
            <div class="share-vendors-items">
              <span class="title-share-vendors">Поделиться:</span>
              <div class="fb-share-button" data-href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" data-size="small">
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" ><img src="/image/catalog/product/sharefb.png" class=""></a></div>
              <div class="img-share-vendors-block" data-href="https://www.your-domain.com/your-page.html" data-layout="button_count"><img src="/image/catalog/product/sharetw.png" class=""></div> 
            </div>
          </div>
        </div>



        </div>



        <!--Правая колонка с личной инфой ВЕНДОРА START-->

        <div class="col-sm-6">

<!--           <div class="btn-group">
            <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
          </div> -->

          <ul class="list-unstyled">
          </ul>

          <div id="product">

            <div class="form-group">
              <!-- <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
              <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" /> -->
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <br />
              <!-- <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button> -->
            </div>
          </div>



            <div class="must-info-vendor">
              <div class="inside-must-info">

              <!-- БЛОК ВЫВОДА АДРЕСА START -->
                <?php if($upc) { ?>
                  <div class="block-adres-vendor block-inside-vendor">
                    <div class="block-img-adres-row block-img-icon-vendor"><img src="/image/catalog/icon-svg/location.svg" class="icon-img-adres icon-img-fon"></div>
                    <div class="content-adres-vendor block-with-cont-vendor"><span class="title-adress-zag">Адрес </span><span class="text-adres"><?php echo $upc ?></span> <a href="https://www.google.com/maps/place/<?php echo $upc ?>" class="link-adres-vendor-maps" target="_blank">Посмотреть на карте</a></div>
                  </div>
                <?php } ?>
                <!-- БЛОК ВЫВОДА АДРЕСА END -->


                <!-- БЛОК ВЫВОДА ТЕЛЕФОНОВ START -->
                <?php if ((!empty($ean)) || (!empty($jan))) { ?>
                  <div class="block-tel-vendor block-inside-vendor">
                    <div class="block-img-tel-row block-img-icon-vendor"><img src="/image/catalog/icon-svg/phone.svg" class="icon-img-tel icon-img-fon"></div>
                    <div class="content-tel-vendor block-with-cont-vendor"><span class="title-adress-zag">Телефон </span>
                      <span class="text-tel">
                        <?php if($ean) { ?><a href="tel:<?php echo $ean ?>" class="tel-link-vendors"><?php echo $ean ?></a><?php } ?>
                        <?php if($jan) { ?><a href="tel:<?php echo $jan ?>" class="tel-link-vendors"><?php echo $jan ?></a><?php } ?>
                      </span> 
                    </div>
                  </div>
                <?php } ?>
                <!-- БЛОК ВЫВОДА ТЕЛЕФОНОВ END -->

                <?php if ((!empty($isbn)) || (!empty($mpn))) { ?>
                  <div class="block-soc-vendor block-inside-vendor">
                    <div class="block-img-soc-row block-img-icon-vendor"><img src="/image/catalog/icon-svg/internet.svg" class="icon-img-soc icon-img-fon"></div>
                    <div class="content-soc-vendor block-with-cont-vendor"><span class="title-adress-zag">Соц.сети </span>
                      <span class="text-tel">
                        <?php if($isbn) { ?><a href="<?php echo $isbn ?>" target="_blank" class="soc-link-vendors"><img src="/image/catalog/product/facebook.png" class="icon-img-soc-link"> Facebook</a><?php } ?>
                        <?php if($mpn) { ?><a href="<?php echo $mpn ?>" target="_blank" class="soc-link-vendors link-vendor-second"><img src="/image/catalog/product/instagram.png" class="icon-img-soc-link"> Instagram</a><?php } ?>
                      </span> 
                    </div>
                  </div>
                <?php } ?>

                <div class="block-write-vendor block-inside-vendor">

                  <div class="block-img-soc-row block-img-icon-vendor"><img src="/image/catalog/icon-svg/mail.svg" class="icon-img-soc icon-img-fon"></div>
                  <div class="content-soc-vendor block-with-cont-vendor">
                    <button class="write-vendors">написать <?php if ($category_id == '77') { echo 'ресторану';} else {echo 'вендору';} ?></button>
                  </div>

                </div>
              </div>
            </div>



        </div>

        <!--Правая колонка с личной инфой ВЕНДОРА END-->

        

        <div class="col-sm-12">
          <hr class="line-row-blue">
        </div>

</div>

      <div class="row">

        <div class="col-sm-12">

          <!--ТАБЫ ПЕРКЛЮЧЕНИЯ ДЛЯ ГАЛЕРЕИ И ОТЗЫВОВ START-->
          <ul class="nav nav-tabs" id="nav-link">

            <?php // if ($attribute_groups) { ?>
            <!-- <li class="active"><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li> -->
            <?php // } ?>

            <li class="active"><a href="#gallery-vendors" data-toggle="tab">Фотографии <?php echo $heading_title ?></a></li>

            <?php if ($review_status) { ?>
            <li class="review-link-tab"><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?>

	<?php if ($similar_products) { ?>
	<li><a href="#tab-similar" data-toggle="tab"><?php echo $tab_similar; ?></a></li>
	<?php } ?>
			
          </ul>

          <div class="btn-write-otzyv"><button onclick="$('a[href=\'#tab-review\']').trigger('click');" class="click-otzyvy">написать отзыв</button></div>

          <!--ТАБЫ ПЕРКЛЮЧЕНИЯ ДЛЯ ГАЛЕРЕИ И ОТЗЫВОВ END-->




          <div class="tab-content">

            <!-- КОНТЕНТ ГАЛЛЕРЕИ START -->

            <div class="tab-pane active" id="gallery-vendors">
              <?php if ($thumb || $images) { ?>

                <div id="masonryGrid">
                  <div class="grid thumbnails">
                    <?php foreach ($images as $image) { ?>
                    <div class="grid-item">
                    <div class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
                    </div>
                  <?php } ?>
                  </div>
                </div>
              <?php } ?>
            </div>

            <script type="text/javascript">
              
              // init Masonry
            var $grid = $('.grid').masonry({
              // options...
              itemSelector: '.grid-item',
              isFitWidth: true,
              columnWidth: 1
            });

            // layout Masonry after each image loads
            $grid.imagesLoaded().progress( function() {
              $grid.masonry('layout');
            });

            </script>

            <!-- КОНТЕНТ ГАЛЛЕРЕИ END -->



            <!-- КОНТЕНТ ОТЗЫВОВ START -->

            <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h2><?php echo $text_write; ?></h2>
                <?php if ($review_guest) { ?>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>

            <!-- КОНТЕНТ БЛОК ОТЗЫВОВ END -->


          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-sm-12">
          <div class="block-description">
            <?php echo $description ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <?php // echo $content_bottom; ?>
        </div>
      </div>





      <?php if ($products) { ?>
      <div class="related-vendors"><?php echo $text_related; ?></div>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>
        <div class="col-sm-3">
          <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <div class="name-related-vendors"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>


              <?php if ($product['rating']) { ?>

                <div class="rating-block-with-cont">
                  <div class="rating rating-related-vendors">
                    <?php for ($j = 1; $j <= 5; $j++) { ?>
                    <?php if ($product['rating'] < $j) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } ?>
                    <?php } ?>
                  </div>
                  <div class="text-rating-related-products"><?php echo $product['reviews'] . ' отзыв'; ?></div>

                  <div class="btn-to-wishist">
                    <button type="button" data-toggle="tooltip" class="btn btn-default wish-btn-vendors" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><img src="/image/catalog/heart.svg" class="like-img like-img-cat"></button>
                  </div>
                </div>

              <?php } else { ?>
                <div class="rating-block-with-cont">
                  <div class="rating rating-related-vendors">
                    <?php for ($j = 1; $j <= 5; $j++) { ?>
                    <?php if ($product['rating'] < $j) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } ?>
                    <?php } ?>
                  </div>
                  <div class="text-rating-related-products"><?php echo $product['reviews'] . ' отзыв'; ?></div>

                  <div class="btn-to-wishist">
                    <button type="button" data-toggle="tooltip" class="btn btn-default wish-btn-vendors" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><img src="/image/catalog/heart.svg" class="like-img like-img-cat"></button>
                  </div>
                </div>
              <?php } ?>

              <?php if ($product['price']) { ?>
              <p class="price">
                <span class="sred-check"><?php echo 'Средний чек:' ?></span>
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
          </div>
        </div>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
      </div>
    <?php echo $column_right; ?></div>
</div>






  <!-- Load Facebook SDK for JavaScript -->






<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
