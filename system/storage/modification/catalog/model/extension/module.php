<?php
class ModelExtensionModule extends Model {

	public function getModulesByCode($code) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "module` WHERE `code` = '" . $this->db->escape($code) . "' ORDER BY `name`");

		return $query->rows;
	}
			
	public function getModule($module_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE module_id = '" . (int)$module_id . "'");
		
		if ($query->row) {
			return json_decode($query->row['setting'], true);
		} else {
			return array();	
		}
	}		

	public function getModuleText($module_name) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE name = '" . $module_name . "'");
     
        if ($query->row) {
            return json_decode($query->row['setting'], true);
        } else {
            return array(); 
        }
    }
    
}