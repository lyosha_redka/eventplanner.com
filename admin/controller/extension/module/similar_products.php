<?php
define('EXTENSION_NAME',            'Similar Products');
define('EXTENSION_VERSION',         '4.1.7');
define('EXTENSION_ID',              '3449');
define('EXTENSION_COMPATIBILITY',   'OpenCart 2.3.x.x');
define('EXTENSION_STORE_URL',       'http://www.opencart.com/index.php?route=extension/extension/info&extension_id=' . EXTENSION_ID);
define('EXTENSION_PURCHASE_URL',    'http://www.opencart.com/index.php?route=extension/purchase&extension_id=' . EXTENSION_ID);
define('EXTENSION_SUPPORT_EMAIL',   'support@opencart.ee');
define('EXTENSION_SUPPORT_FORUM',   'http://forum.opencart.com/viewtopic.php?f=123&t=42624');
define('OTHER_EXTENSIONS',          'http://www.opencart.com/index.php?route=extension/extension&filter_username=bull5-i');
define('EXTENSION_MIN_PHP_VERSION', '5.3.0');

class ControllerExtensionModuleSimilarProducts extends Controller {
	private $error = array();
	protected $alert = array(
		'error'     => array(),
		'warning'   => array(),
		'success'   => array(),
		'info'      => array()
	);

	private static $config_defaults = array(
		'sp_installed'              => 1,
		'sp_installed_version'      => EXTENSION_VERSION,
		'sp_status'                 => 0,
		'sp_auto_select'            => 0,
		'sp_product_sort_order'     => 0,
		'sp_leaves_only'            => 1,
		'sp_substr_start'           => 0,
		'sp_substr_length'          => 5,
		'sp_custom_string'          => "",
		'sp_remove_sql_changes'     => 0,
		'sp_apply_to'               => array(),
		'sp_services'               => "W10=",
		'sp_as'                     => "WyIwIl0=",
	);

	private static $module_defaults = array(
		'module_id'           => '',
		'name'                => '',
		'names'               => array(),
		'show_in_tab'         => '0',
		'products_per_page'   => '4',
		'limit'               => '24',
		'image_width'         => '80',
		'image_height'        => '80',
		'stock_only'          => '0',
		'lazy_load'           => '1',
		'status'              => '0',
	);

	private static $language_texts = array(
		// Texts
		'text_enabled', 'text_disabled', 'text_yes', 'text_no', 'text_toggle_navigation', 'text_legal_notice', 'text_license', 'text_extension_information',
		'text_terms', 'text_license_text', 'text_support_subject', 'text_faq', 'text_random', 'text_most_viewed', 'text_date_added', 'text_date_modified', 'text_name',
		'text_sort_order', 'text_model', 'text_quantity', 'text_no_modules', 'text_saving', 'text_deleting', 'text_upgrading', 'text_off', 'text_tags', 'text_category',
		'text_manufacturer', 'text_category_manufacturer', 'text_name_fragment', 'text_model_fragment', 'text_name_custom_string', 'text_model_custom_string',
		'text_change_product_settings', 'text_no_products', 'text_all_products', 'text_all_empty_products', 'text_all_category_products',
		'text_all_manufacturer_products', 'text_selected_products', 'text_remove', 'text_autocomplete', 'text_product_layout', 'text_confirm_delete',
		'text_are_you_sure', 'text_edit_module', 'text_no_records_found', 'text_loading', 'text_canceling', 'text_opening', 'text_default_module_name',
		// Tabs
		'tab_settings', 'tab_modules', 'tab_support', 'tab_about', 'tab_general', 'tab_faq', 'tab_services', 'tab_changelog', 'tab_extension',
		// Buttons
		'button_save', 'button_apply', 'button_cancel', 'button_close', 'button_upgrade', 'button_add_module', 'button_remove', 'button_delete', 'button_refresh',
		'button_general_settings', 'button_purchase_license',
		// Help texts
		'help_remove_sql_changes', 'help_auto_select', 'help_name_fragment', 'help_custom_string', 'help_leaves_only', 'help_stock_only', 'help_lazy_load',
		'help_change_product_settings', 'help_show_in_tab', 'help_purchase_additional_licenses',
		// Entries
		'entry_installed_version', 'entry_extension_status', 'entry_name', 'entry_show_in_tab', 'entry_limit', 'entry_image_width', 'entry_image_height',
		'entry_position', 'entry_status', 'entry_product_sort_order', 'entry_products_per_page', 'entry_stock_only', 'entry_lazy_load', 'entry_auto_select',
		'entry_leaves_only', 'entry_substr_start', 'entry_substr_length', 'entry_custom_string', 'entry_remove_sql_changes', 'entry_products', 'entry_extension_name',
		'entry_extension_compatibility', 'entry_extension_store_url', 'entry_copyright_notice', 'entry_active_on', 'entry_inactive_on',
		// Errors
		'error_module_name', 'error_positive_integer', 'error_ajax_request'
	);

	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->helper('sp');
		$this->load->language('extension/module/similar_products');
		$this->load->model('extension/module/similar_products');
	}

	public function index() {
		$this->document->addStyle('view/stylesheet/sp/custom.min.css');

		$this->document->addScript('view/javascript/sp/custom.min.js');

		$this->document->setTitle($this->language->get('extension_name'));

		$this->load->model('setting/setting');
		$this->load->model('extension/module');

		$ajax_request = isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

		if (isset($this->request->get['module_id'])) {
			$module_id = $this->request->get['module_id'];
		} else {
			$module_id = null;
		}

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && !$ajax_request) {
			if (!is_null($module_id)) {
				if ($this->validateModuleForm($this->request->post)) {
					$this->model_extension_module->editModule($module_id, $this->request->post);

					$this->session->data['success'] = sprintf($this->language->get('text_success_update_module'), $this->request->post['name']);

					$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
				}
			} else {
				if ($this->validateForm($this->request->post)) {
					$original_settings = $this->model_setting_setting->getSetting('sp');

					foreach (self::$config_defaults as $setting => $default) {
						$value = $this->config->get($setting);
						if ($value === null) {
							$original_settings[$setting] = $default;
						}
					}

					$modules = isset($this->request->post['modules']) ? $this->request->post['modules'] : array();
					unset($this->request->post['modules']);

					if (isset($this->request->post['sp_apply_to']['products']) && $this->request->post['sp_apply_to']['products'] != "") {
						$this->model_extension_module_similar_products->applyMassChange($this->request->post);
					}

					$settings = array_merge($original_settings, $this->request->post);
					$settings['sp_installed_version'] = $original_settings['sp_installed_version'];
					$settings['sp_apply_to'] = array();

					$this->model_setting_setting->editSetting('sp', $settings);

					$previous_modules = $this->model_extension_module->getModulesByCode('similar_products');
					$previous_modules = array_remap_key_to_id('module_id', $previous_modules);

					foreach ($modules as $module) {
						if (!empty($module['module_id'])) {
							$module_id = $module['module_id'];
							unset($previous_modules[$module_id]);
							$this->model_extension_module->editModule($module_id, $module);
						} else {
							$this->model_extension_module->addModule('similar_products', $module);

							$module_id = $this->db->getLastId();
							$module['module_id'] = $module_id;
							$this->model_extension_module->editModule($module_id, $module);
						}
					}

					// Delete any modules left over
					foreach ($previous_modules as $module_id => $module) {
						$this->model_extension_module->deleteModule($module_id);
					}

					$this->session->data['success'] = $this->language->get('text_success_update');

					$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
				}
			}
		} else if ($this->request->server['REQUEST_METHOD'] == 'POST' && $ajax_request) {
			$response = array();

			if (!is_null($module_id)) {
				if ($this->validateModuleForm($this->request->post)) {
					$this->model_extension_module->editModule($module_id, $this->request->post);

					$this->alert['success']['updated'] = sprintf($this->language->get('text_success_update_module'), $this->request->post['name']);
				}
			} else {
				if ($this->validateForm($this->request->post)) {
					$original_settings = $this->model_setting_setting->getSetting('sp');

					foreach (self::$config_defaults as $setting => $default) {
						$value = $this->config->get($setting);
						if ($value === null) {
							$original_settings[$setting] = $default;
						}
					}

					$modules = isset($this->request->post['modules']) ? $this->request->post['modules'] : array();
					unset($this->request->post['modules']);

					if (isset($this->request->post['sp_apply_to']['products']) && $this->request->post['sp_apply_to']['products'] != "") {
						$this->model_extension_module_similar_products->applyMassChange($this->request->post);
					}

					$settings = array_merge($original_settings, $this->request->post);
					$settings['sp_installed_version'] = $original_settings['sp_installed_version'];
					$settings['sp_apply_to'] = array();

					$this->model_setting_setting->editSetting('sp', $settings);

					$previous_modules = $this->model_extension_module->getModulesByCode('similar_products');
					$previous_modules = array_remap_key_to_id('module_id', $previous_modules);

					foreach ($modules as $idx => $module) {
						if (!empty($module['module_id'])) {
							$module_id = $module['module_id'];
							unset($previous_modules[$module_id]);
							$this->model_extension_module->editModule($module_id, $module);
						} else {
							$this->model_extension_module->addModule('similar_products', $module);

							$module_id = $this->db->getLastId();

							$module['module_id'] = $module_id;
							$this->model_extension_module->editModule($module_id, $module);

							$response['values']['modules'][$idx]['module_id'] = $module_id;
						}
					}

					// Delete any modules left over
					foreach ($previous_modules as $module_id => $module) {
						$this->model_extension_module->deleteModule($module_id);
					}

					$this->alert['success']['updated'] = $this->language->get('text_success_update');
				} else {
					if (!$this->checkVersion()) {
						$response['reload'] = true;
					}
				}
			}

			$response = array_merge($response, array("errors" => $this->error), array("alerts" => $this->alert));

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
			return;
		}

		$db_structure_ok = $this->checkVersion() && $this->model_extension_module_similar_products->checkDatabaseStructure($this->alert);

		$this->alert = array_merge($this->alert, $this->model_extension_module_similar_products->getAlerts());

		$this->checkPrerequisites();

		$this->checkVersion();

		$data['heading_title'] = $this->language->get('extension_name');
		$data['text_other_extensions'] = sprintf($this->language->get('text_other_extensions'), OTHER_EXTENSIONS);

		foreach (self::$language_texts as $text) {
			$data[$text] = $this->language->get($text);
		}

		$data['ext_name'] = EXTENSION_NAME;
		$data['ext_version'] = EXTENSION_VERSION;
		$data['ext_id'] = EXTENSION_ID;
		$data['ext_compatibility'] = EXTENSION_COMPATIBILITY;
		$data['ext_store_url'] = EXTENSION_STORE_URL;
		$data['ext_purchase_url'] = EXTENSION_PURCHASE_URL;
		$data['ext_support_email'] = EXTENSION_SUPPORT_EMAIL;
		$data['ext_support_forum'] = EXTENSION_SUPPORT_FORUM;
		$data['other_extensions_url'] = OTHER_EXTENSIONS;
		$data['oc_version'] = VERSION;

		$data['alert_icon'] = function($type) {
			$icon = "";
			switch ($type) {
				case 'error':
					$icon = "fa-times-circle";
					break;
				case 'warning':
					$icon = "fa-exclamation-triangle";
					break;
				case 'success':
					$icon = "fa-check-circle";
					break;
				case 'info':
					$icon = "fa-info-circle";
					break;
				default:
					break;
			}
			return $icon;
		};

		if (!is_null($module_id) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($module_id);
			if (!$module_info) {
				$this->response->redirect($this->url->link('extension/module/similar_products', 'token=' . $this->session->data['token'], true));
				return;
			}
		} else {
			$module_info = null;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
			'active'    => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_extension'),
			'href'      => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true),
			'active'    => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('extension_name'),
			'href'      => $this->url->link('extension/module/similar_products', 'token=' . $this->session->data['token'], true),
			'active'    => is_null($module_id)
		);

		if (!is_null($module_id)) {
			$module_name = (!empty($module_info['name'])) ? $module_info['name'] : ((!empty($this->request->post['name'])) ? $this->request->post['name'] : EXTENSION_NAME);
			$data['breadcrumbs'][] = array(
				'text'      => $module_name,
				'href'      => $this->url->link('extension/module/similar_products', 'token=' . $this->session->data['token'] . '&module_id=' . $module_id, true),
				'active'    => true
			);
		}

		$data['save'] = $this->url->link('extension/module/similar_products', 'token=' . $this->session->data['token'] . (!is_null($module_id) ? '&module_id=' . $module_id : ''), true);
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);
		$data['delete'] = $this->url->link('extension/module/delete', 'token=' . $this->session->data['token'] . (!is_null($module_id) ? '&module_id=' . $module_id : ''), true);
		$data['upgrade'] = $this->url->link('extension/module/similar_products/upgrade', 'token=' . $this->session->data['token'], true);
		$data['general_settings'] = $this->url->link('extension/module/similar_products', 'token=' . $this->session->data['token'], true);
		$data['extension_installer'] = $this->url->link('extension/installer', 'token=' . $this->session->data['token'], true);
		$data['services'] = html_entity_decode($this->url->link('extension/module/similar_products/services', 'token=' . $this->session->data['token'], true));
		$data['autocomplete'] = html_entity_decode($this->url->link('extension/module/similar_products/autocomplete', 'type=%TYPE%&query=%QUERY&token=' . $this->session->data['token'], true));

		$data['update_pending'] = !$this->checkVersion();

		if (!$data['update_pending']) {
			$this->updateEventHooks();
		} else if (!is_null($module_id)) {
			$this->response->redirect($this->url->link('extension/module/similar_products', 'token=' . $this->session->data['token'], true));
			return;
		}

		$data['ssl'] = (
				(int)$this->config->get('config_secure') ||
				isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ||
				!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ||
				!empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on'
			) ? 's' : '';

		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		$data['languages'] = array_remap_key_to_id('language_id', $languages);
		$data['default_language'] = $this->config->get('config_language_id');

		$data['default_image_width'] = $this->config->get($this->config->get('config_theme') . '_image_related_width');
		$data['default_image_height'] = $this->config->get($this->config->get('config_theme') . '_image_related_height');

		$this->load->model('catalog/category');
		$categories = $this->model_catalog_category->getCategories(array('sort' => 'name'));
		$data['categories'] = array_remap_key_to_id('category_id', $categories);

		$this->load->model('catalog/manufacturer');
		$manufacturers = $this->model_catalog_manufacturer->getManufacturers(array('sort' => 'name'));
		$data['manufacturers'] = array_remap_key_to_id('manufacturer_id', $manufacturers);

		$data['installed_version'] = $this->installedVersion();

		if (is_null($module_id)) {
			# Loop through all settings for the post/config values
			foreach (array_keys(self::$config_defaults) as $setting) {
				if (isset($this->request->post[$setting])) {
					$data[$setting] = $this->request->post[$setting];
				} else {
					$data[$setting] = $this->config->get($setting);
					if ($data[$setting] === null) {
						if (!isset($this->alert['warning']['unsaved']) && $this->checkVersion())  {
							$this->alert['warning']['unsaved'] = $this->language->get('error_unsaved_settings');
						}
						if (isset(self::$config_defaults[$setting])) {
							$data[$setting] = self::$config_defaults[$setting];
						}
					}
				}
			}
			if (isset($this->request->post['modules'])) {
				$data['modules'] = $this->request->post['modules'];
			} else {
				$modules = $this->model_extension_module->getModulesByCode('similar_products');

				foreach ($modules as $idx => $module) {
					$module_settings = json_decode($module['setting'], true);
					unset($module['setting']);
					$module = array_merge($module, $module_settings);

					foreach (array_keys(self::$module_defaults) as $setting) {
						if (!isset($module[$setting])) {
							$module[$setting] = self::$module_defaults[$setting];

							if (!isset($this->alert['warning']['unsaved']) && $this->checkVersion())  {
								$this->alert['warning']['unsaved'] = $this->language->get('error_unsaved_settings');
							}
						}
						$modules[$idx] = $module;
					}
				}

				$data['modules'] = $modules;
			}

			foreach (array_keys(self::$module_defaults) as $setting) {
				$data["sp_m_$setting"] = self::$module_defaults[$setting];
			}

			$this->load->model('setting/store');

			$stores = $this->model_setting_store->getStores();

			$data['stores'] = array();

			$data['stores'][0] = array(
				'name' => $this->config->get('config_name'),
				'url'  => HTTP_CATALOG
			);

			foreach ($stores as $store) {
				$data['stores'][$store['store_id']] = array(
					'name' => $store['name'],
					'url'  => $store['url']
				);
			}
		} else {
			foreach (array_keys(self::$module_defaults) as $setting) {
				if (isset($this->request->post[$setting])) {
					$data[$setting] = $this->request->post[$setting];
				} else if (isset($module_info[$setting])) {
					$data[$setting] = $module_info[$setting];
				} else {
					$data[$setting] = self::$module_defaults[$setting];
					if (!isset($this->alert['warning']['unsaved']) && $this->checkVersion())  {
						$this->alert['warning']['unsaved'] = $this->language->get('error_unsaved_settings');
					}
				}
			}
			$data['module_id'] = $module_id;

			$modules = $this->model_extension_module->getModulesByCode('similar_products');

			$tab_position_used = 0;

			foreach ($modules as $idx => $module) {
				$module_settings = json_decode($module['setting'], true);

				if ((int)$module_settings['show_in_tab'] && $module_id != $module['module_id']) {
					$tab_position_used = 1;
					break;
				}
			}
			$data['tab_position_used'] = $tab_position_used;
		}

		if (isset($this->session->data['error'])) {
			$this->error = $this->session->data['error'];

			unset($this->session->data['error']);
		}

		if (isset($this->error['warning'])) {
			$this->alert['warning']['warning'] = $this->error['warning'];
		}

		if (isset($this->error['error'])) {
			$this->alert['error']['error'] = $this->error['error'];
		}

		if (isset($this->session->data['success'])) {
			$this->alert['success']['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		}

		$data['errors'] = $this->error;

		$data['token'] = $this->session->data['token'];

		$data['alerts'] = $this->alert;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		if (is_null($module_id)) {
			$template = 'extension/module/similar_products';
		} else {
			$template = 'extension/module/similar_products_module';
		}

		$this->response->setOutput($this->load->view($template, $data));
	}

	public function install() {
		$this->registerEventHooks();

		$this->model_extension_module_similar_products->applyDatabaseChanges();

		$this->load->model('setting/setting');
		$this->model_setting_setting->editSetting('sp', self::$config_defaults);
	}

	public function uninstall() {
		$this->removeEventHooks();

		if ($this->config->get("sp_remove_sql_changes")) {
			$this->model_extension_module_similar_products->revertDatabaseChanges();
		}

		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('sp');

		$this->load->model('extension/module');
		$this->model_extension_module->deleteModulesByCode('similar_products');
	}

	public function upgrade() {
		$ajax_request = isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

		$response = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateUpgrade()) {
			$this->load->model('setting/setting');

			if ($this->model_extension_module_similar_products->upgradeDatabaseStructure($this->installedVersion(), $this->alert)) {
				$settings = array();

				// Go over all settings, add new values and remove old ones
				foreach (self::$config_defaults as $setting => $default) {
					$value = $this->config->get($setting);
					if ($value === null) {
						$settings[$setting] = $default;
					} else {
						$settings[$setting] = $value;
					}
				}

				$settings['sp_installed_version'] = EXTENSION_VERSION;

				$this->model_setting_setting->editSetting('sp', $settings);

				$this->session->data['success'] = sprintf($this->language->get('text_success_upgrade'), EXTENSION_VERSION);
				$this->alert['success']['upgrade'] = sprintf($this->language->get('text_success_upgrade'), EXTENSION_VERSION);

				$response['success'] = true;
				$response['reload'] = true;
			} else {
				$this->alert = array_merge($this->alert, $this->model_extension_module_similar_products->getAlerts());
				$this->alert['error']['database_upgrade'] = $this->language->get('error_upgrade_database');
			}
		}

		$response = array_merge($response, array("errors" => $this->error), array("alerts" => $this->alert));

		if (!$ajax_request) {
			$this->session->data['errors'] = $this->error;
			$this->session->data['alerts'] = $this->alert;
			$this->response->redirect($this->url->link('extension/module/similar_products', 'token=' . $this->session->data['token'], true));
		} else {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
			return;
		}
	}

	public function services() {
		$services = base64_decode($this->config->get('sp_services'));
		$response = json_decode($services, true);
		$force = isset($this->request->get['force']) && (int)$this->request->get['force'];

		if ($response && isset($response['expires']) && $response['expires'] >= strtotime("now") && !$force) {
			$response['cached'] = true;
		} else {
			$url = "http://www.opencart.ee/services/?eid=" . EXTENSION_ID . "&info=true&general=true";
			$hostname = (!empty($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : '' ;

			if (function_exists('curl_init')) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
				curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				curl_setopt($ch, CURLOPT_USERAGENT, base64_encode("curl " . EXTENSION_ID));
				curl_setopt($ch, CURLOPT_REFERER, $hostname);
				$json = curl_exec($ch);
			} else {
				$json = false;
			}

			if ($json !== false) {
				$this->load->model('setting/setting');
				$settings = $this->model_setting_setting->getSetting('sp');
				$settings['sp_services'] = base64_encode($json);
				$this->model_setting_setting->editSetting('sp', $settings);
				$response = json_decode($json, true);
			} else {
				$response = array();
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
	}

	private function registerEventHooks() {
		// $this->load->model('extension/event');
		// $this->model_extension_event->addEvent('sp.product.delete', 'pre.admin.product.delete', 'extension/module/similar_products/hook');
	}

	private function removeEventHooks() {
		// $this->load->model('extension/event');
		// $this->model_extension_event->deleteEvent('sp.product.delete');
	}

	private function updateEventHooks() {
		$this->load->model('extension/event');

		$event_hooks = array(
			// 'sp.product.delete'         => array('trigger' => 'pre.admin.product.delete',           'action' => 'extension/module/similar_products/hook'),
		);

		foreach ($event_hooks as $code => $hook) {
			$event = $this->model_extension_event->getEvent($code, $hook['trigger'], $hook['action']);

			if (!$event) {
				$this->model_extension_event->addEvent($code, $hook['trigger'], $hook['action']);

				if (empty($this->alert['success']['hooks_updated'])) {
					$this->alert['success']['hooks_updated'] = $this->language->get('text_success_hooks_update');
				}
			}
		}

		// Delete old triggers
		$query = $this->db->query("SELECT `code` FROM " . DB_PREFIX . "event WHERE `code` LIKE 'sp.%'");
		$events = array_keys($event_hooks);

		foreach ($query->rows as $row) {
			if (!in_array($row['code'], $events)) {
				$this->model_extension_event->deleteEvent($row['code']);

				if (empty($this->alert['success']['hooks_updated'])) {
					$this->alert['success']['hooks_updated'] = $this->language->get('text_success_hooks_update');
				}
			}
		}
	}

	public function autocomplete() {
		if ($this->request->server['REQUEST_METHOD'] == 'GET' && isset($this->request->get['type'])) {
			$response = array();
			switch ($this->request->get['type']) {
				case 'product':
					$this->load->model('catalog/product');

					$results = array();

					if (isset($this->request->get['query'])) {
						$filter_data = array(
							'filter_name'   => $this->request->get['query'],
							'sort'          => 'pd.name',
							'start'         => 0,
							'limit'         => 20,
						);

						$results = $this->model_catalog_product->getProducts($filter_data);
					}

					foreach ($results as $result) {
						$result['name'] = html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8');
						$response[] = array(
							'value'     => $result['name'],
							'tokens'    => explode(' ', $result['name']),
							'id'        => $result['product_id'],
							'model'     => $result['model']
						);
					}
					break;
				case 'category':
					$this->load->model('catalog/category');

					$results = array();

					if (isset($this->request->get['query'])) {
						$filter_data = array(
							'filter_name'   => $this->request->get['query'],
						);

						$results = $this->model_catalog_category->getCategories($filter_data);

						if (stripos($this->language->get('text_none'), $this->request->get['query']) !== false) {
							$response[] = array(
									'value'     => $this->language->get('text_none'),
									'tokens'    => explode(' ', trim(str_replace('---', '', $this->language->get('text_none')))),
									'id'        => '*',
									'path'      => '',
									'full_name' => $this->language->get('text_none')
								);
						}
					}

					foreach ($results as $result) {
						$result['name'] = html_entity_decode(str_replace('&nbsp;', '', $result['name']), ENT_QUOTES, 'UTF-8');
						$parts = explode('>', $result['name']);
						$last_part = array_pop($parts);

						$response[] = array(
							'value'     => $last_part,
							'tokens'    => explode('>', $result['name']),
							'id'        => $result['category_id'],
							'path'      => $parts ? implode(' > ', $parts) . ' > ' : '',
							'full_name' => $result['name']
						);
					}
					break;
				default:
					break;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
	}

	private function checkPrerequisites() {
		$errors = false;

		if (version_compare(phpversion(), EXTENSION_MIN_PHP_VERSION) < 0) {
			$errors = true;
			$this->alert['error']['php'] = sprintf($this->language->get('error_php_version'), phpversion(), EXTENSION_MIN_PHP_VERSION);
		}

		if (!defined('OCMOD_WORKING')) {
			$this->alert['warning']['ocmod'] = $this->language->get('error_ocmod_script');
		}

		return !$errors;
	}

	private function checkVersion() {
		$errors = false;

		$installed_version = $this->installedVersion();

		if ($installed_version != EXTENSION_VERSION) {
			$errors = true;
			$this->alert['info']['version'] = sprintf($this->language->get('error_version'), EXTENSION_VERSION);
		}

		return !$errors;
	}

	private function validate() {
		$errors = false;

		if (!$this->user->hasPermission('modify', 'extension/module/similar_products')) {
			$errors = true;
			$this->alert['error']['permission'] = $this->language->get('error_permission');
		}

		if (!$errors) {
			$result = $this->checkVersion() && $this->model_extension_module_similar_products->checkDatabaseStructure($this->alert) && $this->checkPrerequisites();
			$this->alert = array_merge($this->alert, $this->model_extension_module_similar_products->getAlerts());
			return $result;
		} else {
			return false;
		}
	}

	private function validateUpgrade() {
		$errors = false;

		if (!$this->user->hasPermission('modify', 'extension/module/similar_products')) {
			$errors = true;
			$this->alert['error']['permission'] = $this->language->get('error_permission');
		}

		return !$errors;
	}

	private function validateForm(&$data) {
		$errors = false;

		if (isset($data['modules'])) {
			foreach ((array)$data['modules'] as $idx => $module) {
				if (isset($module['names'])) {
					foreach ((array)$module['names'] as $language_id => $value) {
						if (!utf8_strlen($value)) {
							$errors = true;
							$this->error['modules'][$idx]['names'][$language_id]['name'] = $this->language->get('error_module_name');
						}
					}
				} else {
					$errors = true;
				}

				if ((int)$module['image_width'] < 0 || (string)((int)$module['image_width']) != $module['image_width']) {
					$errors = true;
					$this->error['modules'][$idx]['image_width'] = $this->language->get('error_positive_integer');
				}

				if ((int)$module['image_height'] < 0 || (string)((int)$module['image_height']) != $module['image_height']) {
					$errors = true;
					$this->error['modules'][$idx]['image_height'] = $this->language->get('error_positive_integer');
				}

				if ((int)$module['products_per_page'] < 0 || (string)((int)$module['products_per_page']) != $module['products_per_page']) {
					$errors = true;
					$this->error['modules'][$idx]['products_per_page'] = $this->language->get('error_positive_integer');
				}
			}
		}

		if ($errors) {
			$this->alert['warning']['warning'] = $this->language->get('error_warning');
		}

		if (!$errors) {
			return $this->validate();
		} else {
			return false;
		}
	}

	private function validateModuleForm(&$data) {
		$errors = false;

		if (isset($data['names'])) {
			foreach ((array)$data['names'] as $language_id => $value) {
				if (!utf8_strlen($value)) {
					$errors = true;
					$this->error['names'][$language_id]['name'] = $this->language->get('error_module_name');
				}
			}
		} else {
			$errors = true;
		}

		if ((int)$data['image_width'] < 0 || (string)((int)$data['image_width']) != $data['image_width']) {
			$errors = true;
			$this->error['image_width'] = $this->language->get('error_positive_integer');
		}

		if ((int)$data['image_height'] < 0 || (string)((int)$data['image_height']) != $data['image_height']) {
			$errors = true;
			$this->error['image_height'] = $this->language->get('error_positive_integer');
		}

		if ((int)$data['products_per_page'] < 0 || (string)((int)$data['products_per_page']) != $data['products_per_page']) {
			$errors = true;
			$this->error['products_per_page'] = $this->language->get('error_positive_integer');
		}

		if ($errors) {
			$this->alert['warning']['warning'] = $this->language->get('error_warning');
		}

		if (!$errors) {
			return $this->validate();
		} else {
			return false;
		}
	}

	private function installedVersion() {
		$installed_version = $this->config->get('sp_installed_version');
		return $installed_version ? $installed_version : '3.2.2';
	}
}
