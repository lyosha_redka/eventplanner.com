<?php echo $header; ?>
<div class="modal fade" id="legal_text" tabindex="-1" role="dialog" aria-labelledby="legal_text_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="legal_text_label"><?php echo $text_terms; ?></h4>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default cancel" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $button_close; ?></button>
			</div>
		</div>
	</div>
</div>
<?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<ul class="breadcrumb bull5i-breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li<?php echo ($breadcrumb['active']) ? ' class="active"' : ''; ?>><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
			<div class="navbar-placeholder">
				<nav class="navbar navbar-bull5i" role="navigation" id="bull5i-navbar">
					<div class="nav-container">

						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bull5i-navbar-collapse">
								<span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<h1 class="bull5i-navbar-brand"><i class="fa fa-files-o fa-fw ext-icon"></i> <?php echo $heading_title; ?></h1>
						</div>
						<div class="collapse navbar-collapse" id="bull5i-navbar-collapse">
							<ul class="nav navbar-nav">
								<li class="active"><a href="#settings" data-toggle="tab"><!-- ko if: general_errors() --><i class="fa fa-exclamation-circle text-danger hidden" data-bind="css:{'hidden': !general_errors()}"></i> <!-- /ko --><?php echo $tab_settings; ?></a></li>
								<li><a href="#modules" data-toggle="tab"><!-- ko if: module_errors() --><i class="fa fa-exclamation-circle text-danger hidden" data-bind="css:{'hidden': !module_errors()}"></i> <!-- /ko --><?php echo $tab_modules; ?></a></li>
								<li><a href="#ext-support" data-toggle="tab"><?php echo $tab_support; ?></a></li>
								<li><a href="#about-ext" data-toggle="tab"><?php echo $tab_about; ?></a></li>
							</ul>
							<div class="nav navbar-nav btn-group navbar-right">
								<?php if ($update_pending) { ?><button type="button" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_upgrade; ?>" class="btn btn-info" id="btn-upgrade" data-url="<?php echo $upgrade; ?>" data-form="#sForm" data-context="#content" data-overlay="#page-overlay" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_upgrading; ?></span>"><i class="fa fa-arrow-circle-up"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_upgrade; ?></span></button><?php } ?>
								<button type="button" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_apply; ?>" class="btn btn-success" id="btn-apply" data-url="<?php echo $save; ?>" data-form="#sForm" data-context="#content" data-vm="ExtVM" data-overlay="#page-overlay" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_saving; ?></span>"<?php echo $update_pending ? ' disabled': ''; ?>><i class="fa fa-check"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_apply; ?></span></button>
								<button type="submit" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_save; ?>" class="btn btn-primary" id="btn-save" data-url="<?php echo $save; ?>" data-form="#sForm" data-context="#content" data-overlay="#page-overlay" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_saving; ?></span>" <?php echo $update_pending ? ' disabled': ''; ?>><i class="fa fa-save"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_save; ?></span></button>
								<a href="<?php echo $cancel; ?>" class="btn btn-default" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_cancel; ?>" id="btn-cancel" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_canceling; ?></span>"><i class="fa fa-ban"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_cancel; ?></span></a>
							</div>
						</div>

					</div>
				</nav>
			</div>
		</div>
	</div>

	<div class="alerts">
		<div class="container-fluid" id="alerts">
			<?php foreach ($alerts as $type => $_alerts) { ?>
				<?php foreach ((array)$_alerts as $alert) { ?>
					<?php if ($alert) { ?>
			<div class="alert alert-<?php echo ($type == "error") ? "danger" : $type; ?> fade in">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="fa <?php echo $alert_icon($type); ?>"></i><?php echo $alert; ?>
			</div>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</div>
	</div>

	<div class="container-fluid bull5i-content bull5i-container">
		<div id="page-overlay" class="bull5i-overlay fade in">
			<div class="page-overlay-progress"><i class="fa fa-refresh fa-spin fa-5x text-muted"></i></div>
		</div>

		<form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="sForm" class="form-horizontal" role="form">
			<div class="tab-content">
				<div class="tab-pane active" id="settings">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-cog fa-fw"></i> <?php echo $tab_settings; ?></h3></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-12">
									<fieldset>
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="sp_status"><?php echo $entry_extension_status; ?></label>
											<div class="col-sm-2 fc-auto-width">
												<select name="sp_status" id="sp_status" data-bind="value: status" class="form-control">
													<option value="1"><?php echo $text_enabled; ?></option>
													<option value="0"><?php echo $text_disabled; ?></option>
												</select>
												<input type="hidden" name="sp_installed" value="1" />
												<input type="hidden" name="sp_installed_version" value="<?php echo $installed_version; ?>" />
												<input type="hidden" name="similar_products_status" data-bind="value: status" />
												<input type="hidden" name="sp_as" data-bind="value: as" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="sp_remove_sql_changes0"><?php echo $entry_remove_sql_changes; ?></label>
											<div class="col-sm-9 col-md-10">
												<label class="radio-inline">
													<input type="radio" name="sp_remove_sql_changes" id="sp_remove_sql_changes1" value="1" data-bind="checked: remove_sql_changes"> <?php echo $text_yes; ?>
												</label>
												<label class="radio-inline">
													<input type="radio" name="sp_remove_sql_changes" id="sp_remove_sql_changes0" value="0" data-bind="checked: remove_sql_changes"> <?php echo $text_no; ?>
												</label>
											</div>
											<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
												<span class="help-block help-text"><?php echo $help_remove_sql_changes; ?></span>
											</div>
										</div>
										<!-- ko if: _sas() == 1 -->
										<div class="form-group">
											<div class="col-sm-offset-3 col-md-offset-2 col-sm-6 col-md-5 col-lg-4">
												<select class="form-control" data-bind="selectedOptions: _as" multiple>
													<?php foreach ($stores as $store_id => $store) { ?>
													<option value="<?php echo $store_id; ?>"><?php echo $store['name']; ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- /ko -->
									</fieldset>
									<fieldset id="sp-mass-change">
										<legend><?php echo $text_change_product_settings; ?></legend>
										<div class="row">
											<div class="col-sm-12 help-container">
												<span class="help-block help-text"><?php echo $help_change_product_settings; ?></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="sp_auto_select"><?php echo $entry_auto_select; ?></label>
											<div class="col-sm-2 fc-auto-width">
												<select name="sp_auto_select" id="sp_auto_select" data-bind="value: auto_select" class="form-control">
													<option value="0"><?php echo $text_off; ?></option>
													<option value="1"><?php echo $text_category; ?></option>
													<option value="2"><?php echo $text_name_fragment; ?></option>
													<option value="3"><?php echo $text_model_fragment; ?></option>
													<option value="4"><?php echo $text_name_custom_string; ?></option>
													<option value="5"><?php echo $text_model_custom_string; ?></option>
													<option value="6"><?php echo $text_tags; ?></option>
													<option value="7"><?php echo $text_manufacturer; ?></option>
													<option value="8"><?php echo $text_category_manufacturer; ?></option>
												</select>
											</div>
											<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
												<span class="help-block help-text"><?php echo $help_auto_select; ?></span>
											</div>
										</div>
										<!-- ko if: auto_select() == '1' || auto_select() == '8' -->
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="sp_leaves_only"><?php echo $entry_leaves_only; ?></label>
											<div class="col-sm-2 fc-auto-width">
												<select name="sp_leaves_only" id="sp_leaves_only" data-bind="value: leaves_only" class="form-control">
													<option value="1"><?php echo $text_yes; ?></option>
													<option value="0"><?php echo $text_no; ?></option>
												</select>
											</div>
											<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
												<span class="help-block help-text"><?php echo $help_leaves_only; ?></span>
											</div>
										</div>
										<!-- /ko -->
										<!-- ko if: auto_select() != '1' && auto_select() != '8' -->
										<input type="hidden" name="sp_leaves_only" data-bind="value: leaves_only">
										<!-- /ko -->
										<!-- ko if: auto_select() == '2' || auto_select() == '3' -->
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="sp_substr_start"><?php echo $entry_substr_start; ?></label>
											<div class="col-sm-2 col-md-1">
												<input name="sp_substr_start" id="sp_substr_start" data-bind="value: substr_start" class="form-control text-right">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="sp_substr_length"><?php echo $entry_substr_length; ?></label>
											<div class="col-sm-2 col-md-1">
												<input name="sp_substr_length" id="sp_substr_length" data-bind="value: substr_length" class="form-control text-right">
											</div>
											<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
												<span class="help-block help-text"><?php echo $help_name_fragment; ?></span>
											</div>
										</div>
										<!-- /ko -->
										<!-- ko if: auto_select() != '2' && auto_select() != '3' -->
										<input type="hidden" name="sp_substr_start" data-bind="value: substr_start">
										<input type="hidden" name="sp_substr_length" data-bind="value: substr_length">
										<!-- /ko -->
										<!-- ko if: auto_select() == '4' || auto_select() == '5' -->
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="sp_custom_string"><?php echo $entry_custom_string; ?></label>
											<div class="col-sm-4 col-md-3">
												<input name="sp_custom_string" id="sp_custom_string" data-bind="value: custom_string" class="form-control">
											</div>
											<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
												<span class="help-block help-text"><?php echo $help_custom_string; ?></span>
											</div>
										</div>
										<!-- /ko -->
										<!-- ko if: auto_select() != '4' && auto_select() != '5' -->
										<input type="hidden" name="sp_custom_string" data-bind="value: custom_string">
										<!-- /ko -->
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="sp_product_sort_order"><?php echo $entry_product_sort_order; ?></label>
											<div class="col-sm-2 fc-auto-width">
												<select name="sp_product_sort_order" id="sp_product_sort_order" data-bind="value: product_sort_order" class="form-control">
													<option value="0"><?php echo $text_sort_order; ?></option>
													<option value="1"><?php echo $text_model; ?></option>
													<option value="2"><?php echo $text_name; ?></option>
													<option value="3"><?php echo $text_quantity; ?></option>
													<option value="4"><?php echo $text_most_viewed; ?></option>
													<option value="5"><?php echo $text_date_added; ?></option>
													<option value="6"><?php echo $text_date_modified; ?></option>
													<option value="7"><?php echo $text_random; ?></option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 col-md-2 control-label" for="apply_to_null"><?php echo $entry_products; ?></label>
											<div class="col-sm-9 col-md-10">
												<label class="radio">
													<input type="radio" value="" name="sp_apply_to[products]" id="apply_to_null" data-bind="checked: products"> <?php echo $text_no_products; ?>
												</label>
												<label class="radio">
													<input type="radio" value="0" name="sp_apply_to[products]" id="apply_to_all" data-bind="checked: products"> <?php echo $text_all_products; ?>
												</label>
												<label class="radio">
													<input type="radio" value="1" name="sp_apply_to[products]" id="apply_to_category" data-bind="checked: products"> <?php echo $text_all_category_products; ?>
												</label>
												<div class="row">
													<div class="col-sm-12">
														<select name="sp_apply_to[category]" data-bind="disable: products() != '1', value: category" class="form-control fc-auto-width">
															<?php foreach ($categories as $cat) { ?>
															<option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['name']; ?></option>
															<?php } ?>
														</select>
													</div>
												</div>
												<label class="radio">
													<input type="radio" value="2" name="sp_apply_to[products]" id="apply_to_manufacturer" data-bind="checked: products"> <?php echo $text_all_manufacturer_products; ?>
												</label>
												<div class="row">
													<div class="col-sm-12">
														<select name="sp_apply_to[manufacturer]" data-bind="disable: products() != '2', value: manufacturer" class="form-control fc-auto-width">
															<?php foreach ($manufacturers as $manuf) { ?>
															<option value="<?php echo $manuf['manufacturer_id']; ?>"><?php echo $manuf['name']; ?></option>
															<?php } ?>
														</select>
													</div>
												</div>
												<label class="radio">
													<input type="radio" value="3" name="sp_apply_to[products]" id="sp_apply_to_selected" data-bind="checked: products"> <?php echo $text_selected_products; ?>
												</label>
												<div class="row">
													<div class="col-sm-6 col-md-5 col-lg-4">
														<input class="form-control typeahead products" placeholder="<?php echo $text_autocomplete; ?>" autocomplete="off" data-method="addSelected" data-bind="typeahead: { options:{ autoSelect: true }, datasets: $root.ta_dataset}">
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12 col-md-8 col-lg-6 multi-row">
														<div class="well well-sm well-scroll-box form-control" data-bind="css: {disabled: products() != '3'}">
														<!-- ko foreach: selected -->
															<div>
																<button type="button" data-bind="click: $parent.removeSelected, tooltip: {container:'body'}" class="btn btn-link btn-xs" data-original-title="<?php echo $text_remove; ?>"><i class="fa fa-minus-circle text-danger"></i></button>
																<!-- ko text: name --><!-- /ko -->
																<input type="hidden" data-bind="attr: {name: 'sp_apply_to[selected][' + $index() + '][product_id]'}, value: id" />
																<input type="hidden" data-bind="attr: {name: 'sp_apply_to[selected][' + $index() + '][name]'}, value: name" />
																<input type="hidden" data-bind="attr: {name: 'sp_apply_to[selected][' + $index() + '][model]'}, value: model" />
															</div>
														<!-- /ko -->
														</div>
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="modules">
					<div class="panel panel-default" id="sp-modules">
						<div class="panel-heading">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#modules-navbar-collapse">
									<span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<h3 class="panel-title"><i class="fa fa-puzzle-piece fa-fw"></i> <?php echo $tab_modules; ?></h3>
							</div>
							<div class="collapse navbar-collapse" id="modules-navbar-collapse">
								<ul class="nav navbar-nav navbar-right">
									<li><button type="button" class="btn btn-primary add-module" data-toggle="tooltip" data-container="body" data-placement="left" title="<?php echo $button_add_module; ?>"><i class="fa fa-plus-circle"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_add_module; ?></span></button></li>
								</ul>
							</div>
						</div>
						<!-- ko if: modules().length == 0 -->
						<div class="panel-body">
							<p><?php echo $text_no_modules; ?></p>
						</div>
						<!-- /ko -->
						<!-- ko if: modules().length > 0 -->
						<div class="alert alert-info panel-margin">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<i class="fa fa-info-circle"></i> <?php echo $text_product_layout; ?>
						</div>
						<!-- /ko -->
						<ul class="list-group">
							<!-- ko foreach: modules -->
							<li class="list-group-item" data-bind="css: {'list-group-item-disabled': !parseInt(status()) }">
								<fieldset>
									<div class="row">
										<div class="col-sm-2 col-xs-4">
											<h2 class="no-margin"># <!-- ko text: module_id() ? module_id() : '?' --><!-- /ko --></h2>
											<input type="hidden" data-bind="attr: {name: 'modules[' + $index() + '][name]'}, value: name() + (show_in_tab() == '1' ? ' (Tab)' : '')">
											<input type="hidden" data-bind="attr: {name: 'modules[' + $index() + '][module_id]'}, value: module_id">
										</div>
										<div class="col-sm-10 col-xs-8">
											<button type="button" class="btn btn-danger remove-module pull-right" data-toggle="tooltip" data-container="body" data-placement="left" title="<?php echo $button_remove; ?>" data-bind="tooltip: {}"><i class="fa fa-minus-circle"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_remove; ?></span></button>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 col-md-4 col-lg-3">
											<div class="form-group no-margin">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $parent.index + '_name<?php echo $default_language; ?>'}, css: {'has-error': names.hasError}"><?php echo $entry_name; ?></label>
												<!-- ko foreach: names -->
												<div data-bind="css: {'multi-row': $index() != 0, 'has-error': name.hasError}">
													<div class="input-group">
														<span class="input-group-addon" data-bind="attr: {title: $root.languages[language_id()].name}, tooltip: {title:$root.languages[language_id()].name}"><img data-bind="attr: {src: $root.languages[language_id()].flag, title: $root.languages[language_id()].name}" /></span>
														<input data-bind="attr: {name: 'modules[' + $parentContext.$index() + '][names][' + language_id() + ']', id: 'sp_' + $parentContext.$index() + '_name' + language_id()}, value: name" class="form-control">
													</div>
												</div>
												<div class="has-error" data-bind="visible: name.hasError && name.errorMsg">
													<span class="help-block" data-bind="text: name.errorMsg"></span>
												</div>
												<!-- /ko -->
											</div>
										</div>
										<div class="col-sm-3 col-md-2 col-lg-2">
											<div class="form-group no-margin">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $index() + '_show_in_tab'}, tooltip:{}" data-toggle="tooltip" data-original-title="<?php echo $help_show_in_tab; ?>"><?php echo $entry_show_in_tab; ?> <i class="fa fa-question-circle text-info"></i></label>
												<select data-bind="attr: {name: 'modules[' + $index() + '][show_in_tab]', id: 'sp_' + $index() + '_show_in_tab'}, value: show_in_tab" class="form-control">
													<!-- ko if: !$root.tabPositionUsed() || show_in_tab() == '1' --><option value="1"><?php echo $text_yes; ?></option><!-- /ko -->
													<option value="0"><?php echo $text_no; ?></option>
												</select>
											</div>
										</div>
										<div class="col-sm-3 col-md-3 col-lg-2">
											<div class="form-group no-margin" data-bind="css: {'has-error': products_per_page.hasError}">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $index() + '_per_page'}"><?php echo $entry_products_per_page; ?></label>
												<input data-bind="attr: {name: 'modules[' + $index() + '][products_per_page]', id: 'sp_' + $index() + '_per_page'}, value: products_per_page" class="form-control text-right">
												<div class="has-error" data-bind="visible: products_per_page.hasError && products_per_page.errorMsg">
													<span class="help-block" data-bind="text: products_per_page.errorMsg"></span>
												</div>
											</div>
										</div>
										<div class="col-sm-2 col-md-1 col-lg-1">
											<div class="form-group no-margin">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $index() + '_limit'}"><?php echo $entry_limit; ?></label>
												<input data-bind="attr: {name: 'modules[' + $index() + '][limit]', id: 'sp_' + $index() + '_limit'}, value: limit" class="form-control text-right">
											</div>
										</div>
										<div class="col-sm-2 col-md-2 col-lg-2">
											<div class="form-group no-margin" data-bind="css: {'has-error': image_width.hasError}">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $index() + '_image_width'}"><?php echo $entry_image_width; ?></label>
												<input data-bind="attr: {name: 'modules[' + $index() + '][image_width]', id: 'sp_' + $index() + '_image_width'}, value: image_width" class="form-control text-right">
												<div class="has-error" data-bind="visible: image_width.hasError && image_width.errorMsg">
													<span class="help-block" data-bind="text: image_width.errorMsg"></span>
												</div>
											</div>
										</div>
										<div class="col-sm-2 col-md-2 col-lg-2">
											<div class="form-group no-margin" data-bind="css: {'has-error': image_height.hasError}">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $index() + '_image_height'}"><?php echo $entry_image_height; ?></label>
												<input data-bind="attr: {name: 'modules[' + $index() + '][image_height]', id: 'sp_' + $index() + '_image_height'}, value: image_height" class="form-control text-right">
												<div class="has-error" data-bind="visible: image_height.hasError && image_height.errorMsg">
													<span class="help-block" data-bind="text: image_height.errorMsg"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group no-margin">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $index() + '_stock_only'}"><?php echo $entry_stock_only; ?> <i class="fa fa-question-circle text-info" data-toggle="tooltip" data-original-title="<?php echo $help_stock_only; ?>"></i></label>
												<select data-bind="attr: {name: 'modules[' + $index() + '][stock_only]', id: 'sp_' + $index() + '_stock_only'}, value: stock_only" class="form-control">
													<option value="1"><?php echo $text_yes; ?></option>
													<option value="0"><?php echo $text_no; ?></option>
												</select>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group no-margin">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $index() + '_lazy_load'}"><?php echo $entry_lazy_load; ?> <i class="fa fa-question-circle text-info" data-toggle="tooltip" data-original-title="<?php echo $help_lazy_load; ?>"></i></label>
												<select data-bind="attr: {name: 'modules[' + $index() + '][lazy_load]', id: 'sp_' + $index() + '_lazy_load'}, value: lazy_load" class="form-control">
													<option value="1"><?php echo $text_yes; ?></option>
													<option value="0"><?php echo $text_no; ?></option>
												</select>
											</div>
										</div>
										<div class="col-sm-3 col-md-2 col-lg-2">
											<div class="form-group no-margin">
												<label class="control-label" data-bind="attr: {for: 'sp_' + $index() + '_status'}"><?php echo $entry_status; ?></label>
												<select data-bind="attr: {name: 'modules[' + $index() + '][status]', id: 'sp_' + $index() + '_status'}, value: status" class="form-control">
													<option value="1"><?php echo $text_enabled; ?></option>
													<option value="0"><?php echo $text_disabled; ?></option>
												</select>
											</div>
										</div>
									</div>
								</fieldset>
							</li>
							<!-- /ko -->
						</ul>
					</div>
				</div>
				<div class="tab-pane" id="ext-support">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#support-navbar-collapse">
									<span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<h3 class="panel-title"><i class="fa fa-phone fa-fw"></i> <?php echo $tab_support; ?></h3>
							</div>
							<div class="collapse navbar-collapse" id="support-navbar-collapse">
								<ul class="nav navbar-nav">
									<li class="active"><a href="#general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
									<li><a href="#faq" data-toggle="tab" title="<?php echo $text_faq; ?>"><?php echo $tab_faq; ?></a></li>
									<li><a href="#services" data-toggle="tab"><?php echo $tab_services; ?></a></li>
								</ul>
							</div>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="general">
									<div class="row">
										<div class="col-sm-12">
											<h3>Getting support</h3>
											<p>I consider support a priority of mine, so if you need any help with your purchase you can contact me in one of the following ways:</p>
											<ul>
												<li>Send an email to <a href="mailto:<?php echo $ext_support_email; ?>?subject='<?php echo $text_support_subject; ?>'"><?php echo $ext_support_email; ?></a></li>
												<li>Post in the <a href="<?php echo $ext_support_forum; ?>" target="_blank">extension forum thread</a> or send me a <a href="http://forum.opencart.com/ucp.php?i=pm&mode=compose&u=17771">private message</a></li>
												<!--li><a href="<?php echo $ext_store_url; ?>" target="_blank">Leave a comment</a> in the extension store comments section</li-->
											</ul>
											<p>I usually reply within a few hours, but can take up to 36 hours.</p>
											<p>Please note that all support is free if it is an issue with the product. Only issues due conflicts with other third party extensions/modules or custom front end theme are the exception to free support. Resolving such conflicts, customizing the extension or doing additional bespoke work will be provided with the hourly rate of <span id="hourly_rate">USD 50 / EUR 40</span>.</p>

											<h4>Things to note when asking for help</h4>
											<p>Please describe your problem in as much detail as possible. When contacting, please provide the following information:</p>
											<ul>
												<li>The OpenCart version you are using: <strong><?php echo $oc_version; ?></strong></li>
												<li>The extension name and version: <strong><?php echo $ext_name; ?> v<?php echo $ext_version; ?></strong></li>
												<li>If you got any error messages, please include them in the message.</li>
												<li>In case the error message is generated by a VQMod or OCMOD cached file, please also attach that file.</li>
											</ul>
											<p>Any additional information that you can provide about the issue is greatly appreciated and will make problem solving much faster.</p>

											<h3 class="page-header">Happy with <?php echo $ext_name; ?>?</h3>
											<p>I would appreciate it very much if you could <a href="<?php echo $ext_store_url; ?>" target="_blank">rate the extension</a> once you've had a chance to try it out. Why not tell everybody how great this extension is by <a href="<?php echo $ext_store_url; ?>" target="_blank">leaving a comment</a> as well.</p>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="alert alert-info">
												<p><?php echo $text_other_extensions; ?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="faq">
									<h3><?php echo $text_faq; ?></h3>
									<ul class="media-list" id="faqs">
										<li class="media">
											<div class="pull-left">
												<i class="fa fa-question-circle fa-4x media-object"></i>
											</div>
											<div class="media-body">
												<h4 class="media-heading">Why isn't the module showing in store front end?</h4>

												<p class="short-answer">Verify that you have updated modification cache. If you are using a custom theme please make sure you have properly integrated the extension with your theme. Check for any <a href="#" class="external-tab-link" data-target="#about-ext">license issues</a>.</p>

												<button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#not_working" data-parent="#faqs">Show the full answer</button>
												<div class="collapse full-answer" id="not_working">
													<p>There may be several causes due to which the module may not appear to be working in the store front end.</p>

													<ol>
														<li>
															<p>Verify that you have updated modification cache (<em>Extensions -> Modifications</em>) and none of the <?php echo $ext_name; ?> OCMOD script files are reporting any errors in the modification log.</p>
															<p>If OCMOD reports errors then these must be addressed.</p>
														</li>

														<li>
															<p>If you are using a custom theme and you are trying to display the module in the tab position please make sure you have properly integrated the extension with your theme. See <a href="#theme_integration" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="theme_integration">"How to integrate the extension with a custom theme?"</a> FAQ below.</p>
														</li>

														<li>
															<p>If you have a multi store installation, you need to verify that proper amount of licenses have been purchased. Check the <a href="#" class="external-tab-link" data-target="#about-ext">About tab</a> License section to see whether <?php echo $ext_name; ?> is activated on all of your stores.</p>
															<p>In case <?php echo $ext_name; ?> is inactive on some of your stores, you will need to purchase additional licenses for each inactive store you wish to enable the extension on.</p>
														</li>
													</ol>

													<p>In case none of the above helped you to get the extension working please contact me on the support email given on the <a href="#" class="external-tab-link" data-target="#general">General Support</a> section.</p>
												</div>
											</div>
										</li>
										<li class="media">
											<div class="pull-left">
												<i class="fa fa-question-circle fa-4x media-object"></i>
											</div>
											<div class="media-body">
												<h4 class="media-heading">How to translate the extension to another language?</h4>

												<p class="short-answer">Copy the extension language files <em>admin/language/english/module/similar_products.php</em> and <em>catalog/language/english/module/similar_products.php</em> to your language folder and translate the string inside the copied files. Additionally translate the language strings found in the <em>system/similar_products.ocmod.xml</em> OCMOD script file.</p>

												<button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#translation" data-parent="#faqs">Show the full answer</button>
												<div class="collapse full-answer" id="translation">
													<h5 class="text-muted no-top-margin">Translating <strong>admin</strong> panel module files</h5>
													<ol>
														<li>
															<p><strong>Copy</strong> the following language file <strong>to YOUR_LANGUAGE folder</strong> under the appropriate location as shown below:</p>
															<div class="btm-mgn">
																<em class="text-muted"><small>FROM:</small></em>
																<ul class="list-unstyled">
																	<li>admin/language/english/module/similar_products.php</li>
																</ul>
																<em class="text-muted"><small>TO:</small></em>
																<ul class="list-unstyled">
																	<li>admin/language/YOUR_LANGUAGE/module/similar_products.php</li>
																</ul>
															</div>
														</li>

														<li>
															<p><strong>Open</strong> the copied <strong>language file</strong> with a text editor such as <a href="http://www.sublimetext.com/">Sublime Text</a> or <a href="http://notepad-plus-plus.org/">Notepad++</a> and <strong>make the required translations</strong>. You can also leave the files in English.</p>
															<p><span class="label label-info">Note</span> You only need to translate the parts that are to the right of the equal sign.</p>
														</li>

														<li>
															<p>Some of the translatable strings are located inside the OCMOD script file <em>system/similar_products.ocmod.xml</em>, so <strong>open the XML file</strong> with a text editor (<strong>not</strong> with a word processor application such as MS Word) and <strong>search</strong> for a <em>file</em> block that edits the <em>admin/language/*/catalog/product.php</em> language file. It should look similar to the following:</p>
															<pre class="prettyprint linenums"><code class="language-xml">    &lt;file name="admin/language/*/catalog/product.php"&gt;
        &lt;operation&gt;
            &lt;search&gt;&lt;![CDATA[
            &lt;?php
            ]]&gt;&lt;/search&gt;
            &lt;add position="after"&gt;&lt;![CDATA[
$_['text_random']                   = 'Random';
$_['text_most_viewed']              = 'Most Viewed';
$_['text_date_added']               = 'Date Added';
$_['text_date_modified']            = 'Date Modified';
$_['text_name']                     = 'Product Name';
$_['text_sort_order']               = 'Sort Order';
$_['text_model']                    = 'Model';
$_['text_quantity']                 = 'Quantity';
$_['text_off']                      = 'Off';
$_['text_tags']                     = 'Product Tags';
$_['text_category']                 = 'Category';
$_['text_name_fragment']            = 'Product Name Fragment';
$_['text_model_fragment']           = 'Product Model Fragment';
$_['text_name_custom_string']       = 'Custom String in Product Name';
$_['text_model_custom_string']      = 'Custom String in Product Model';
$_['text_manufacturer']             = 'Manufacturer';
$_['text_category_manufacturer']    = 'Category &amp; Manufacturer';

$_['entry_sp_auto_select']          = 'Auto select similar';
$_['entry_sp_product_sort_order']   = 'Product sort order';
$_['entry_sp_leaves_only']          = 'Leaves only';
$_['entry_sp_substr_start']         = 'Substring start';
$_['entry_sp_substr_length']        = 'Substring length';
$_['entry_sp_custom_string']        = 'Custom string';
$_['entry_sp_similar_products']     = 'Similar Products';

$_['help_sp_similar_products']      = '(Autocomplete)';
            ]]&gt;&lt;/add&gt;
        &lt;/operation&gt;
    &lt;/file&gt;</code></pre>
														</li>

														<li>
															<p><strong>Translate the string(s)</strong>. You can also leave the strings in English.</p>

															<p><span class="label label-info">Note</span> If you want to quickly familiarize yourself with the simple <a href="https://github.com/opencart/opencart/wiki/Modification-System" target="_blank">OCMOD</a> script syntax, please check out the <a href="https://github.com/opencart/opencart/wiki/Modification-System" target="_blank">official Wiki page</a></p>

															<p>The end result would look similar to the following example:</p>

															<pre class="prettyprint linenums"><code class="language-xml">    &lt;file name="admin/language/*/catalog/product.php"&gt;
        &lt;operation&gt;
            &lt;search&gt;&lt;![CDATA[
            &lt;?php
            ]]&gt;&lt;/search&gt;
            &lt;add position="bottom"&gt;&lt;![CDATA[
$_['text_random']                   = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_most_viewed']              = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_date_added']               = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_date_modified']            = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_name']                     = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_sort_order']               = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_model']                    = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_quantity']                 = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_off']                      = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_tags']                     = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_category']                 = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_name_fragment']            = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_model_fragment']           = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_name_custom_string']       = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_model_custom_string']      = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_manufacturer']             = 'YOUR_LANGUAGE_TRANSLATION';
$_['text_category_manufacturer']    = 'YOUR_LANGUAGE_TRANSLATION';

$_['entry_sp_auto_select']          = 'YOUR_LANGUAGE_TRANSLATION';
$_['entry_sp_product_sort_order']   = 'YOUR_LANGUAGE_TRANSLATION';
$_['entry_sp_leaves_only']          = 'YOUR_LANGUAGE_TRANSLATION';
$_['entry_sp_substr_start']         = 'YOUR_LANGUAGE_TRANSLATION';
$_['entry_sp_substr_length']        = 'YOUR_LANGUAGE_TRANSLATION';
$_['entry_sp_custom_string']        = 'YOUR_LANGUAGE_TRANSLATION';
$_['entry_sp_similar_products']     = 'YOUR_LANGUAGE_TRANSLATION';

$_['help_sp_similar_products']      = 'YOUR_LANGUAGE_TRANSLATION';
            ]]&gt;&lt;/add&gt;
        &lt;/operation&gt;
    &lt;/file&gt;</code></pre>
														</li>
													</ol>

													<h5 class="text-muted">Translating <strong>store front end</strong> module files</h5>
													<ol>
														<li>
															<p><strong>Copy</strong> the following language file <strong>to YOUR_LANGUAGE folder</strong> under the appropriate location as shown below:</p>
															<div class="btm-mgn">
																<em class="text-muted"><small>FROM:</small></em>
																<ul class="list-unstyled">
																	<li>catalog/language/english/module/similar_products.php</li>
																</ul>
																<em class="text-muted"><small>TO:</small></em>
																<ul class="list-unstyled">
																	<li>catalog/language/YOUR_LANGUAGE/module/similar_products.php</li>
																</ul>
															</div>
														</li>

														<li>
															<p><strong>Open</strong> the copied <strong>language file</strong> with a text editor such as <a href="http://www.sublimetext.com/">Sublime Text</a> or <a href="http://notepad-plus-plus.org/">Notepad++</a> and <strong>make the required translations</strong>. You can also leave the files in English.</p>
															<p><span class="label label-info">Note</span> You only need to translate the parts that are to the right of the equal sign.</p>
														</li>

													</ol>
												</div>
											</div>
										</li>
										<li class="media">
											<div class="pull-left">
												<i class="fa fa-question-circle fa-4x media-object"></i>
											</div>
											<div class="media-body">
												<h4 class="media-heading">How to integrate the extension with a custom theme?</h4>

												<p class="short-answer">If the look and feel of the products displayed by this module does not fit your theme then you will have to customize the module specific template files <em>catalog/view/theme/default/template/module/similar_products.tpl</em> and <em>catalog/view/theme/default/template/module/similar_products_products.tpl</em>.</p>

												<button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#theme_integration" data-parent="#faqs">Show the full answer</button>
												<div class="collapse full-answer" id="theme_integration">
													<p>If you are trying to display the Similar Products inside the tab position and the tab does not appear then your custom theme template structure must differ in some way from the default theme. In this case you need to tailor the OCMOD file <em>system/similar_products_default_theme_patch.ocmod.xml</em> search &amp; replace/insert patterns for the <em>product.tpl</em> template file to deal with the structural peculiarities of your custom theme.</p>
													<p>As due to the very nature of a custom theme there does not exist a universal solution. A custom theme may have a different way of displaying things. Take a look at the changes made to the default theme and work out adjustments to the search &amp; replace patterns to suit your theme.</p>
													<p>If you do not know how the OCMOD script works, I kindly suggest you read about it from the OCMOD <a href="https://github.com/opencart/opencart/wiki/Modification-System" target="_blank">wiki pages</a>. OCMOD log (<em>Extensions > Modifications</em> under the <em>Log</em> tab) are helpful for debugging. It will tell you where the script fails (meaning which OCMOD search line it does not find in the referenced file), so you need to adjust that part of the script.</p>
													<p>In case the products displayed by the <?php echo $ext_name; ?> extension do not have a natural look and feel for your theme you need to copy the <em>catalog/view/theme/default/template/module/similar_products.tpl</em> and <em>catalog/view/theme/default/template/module/similar_products_products.tpl</em> template files to the appropriate location under your custom theme folder. After the template files have been copied they need to be custom tailored to the peculiarities of your theme.</p>
													<p>As <?php echo $ext_name; ?> displays a list of products it may be helpful to see how modules such as Bestsellers, Featured, Latest and/or Specials have been integrated with your theme and use the analogy from those modules.</p>
													<p>Should you find yourself in trouble with the changes I can offer commercial custom theme integration service. Please refer to the <a href="#" class="external-tab-link" data-target="#services">Services</a> section.</p>
												</div>
											</div>
										</li>
										<li class="media">
											<div class="pull-left">
												<i class="fa fa-question-circle fa-4x media-object"></i>
											</div>
											<div class="media-body">
												<h4 class="media-heading">How to upgrade the extension?</h4>
												<p class="short-answer">Back up your system, disable the extension, overwrite the current extension files with new ones, refresh the modification cache and click Upgrade on the extension settings page. After upgrade is complete enable the extension again.</p>

												<button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#upgrade" data-parent="#faqs">Show the full answer</button>
												<div class="collapse full-answer" id="upgrade">
													<ol>
														<li>
															<p><strong>Back up your system</strong> before making any upgrades or changes.</p>
															<p><span class="label label-info">Note</span> Although <?php echo $ext_name; ?> does not overwrite any OpenCart core files, it is always a good practice to create a system backup before making any changes to the system.</p>
															<p><span class="label label-danger">Important</span> If the previous <?php echo $ext_name; ?> extension is a VQMod version then delete the vqmod\xml\similar_products.xml file and clear VQMod cache.</p>
														</li>
														<li><strong>Disable</strong> <?php echo $ext_name; ?> <strong>extension</strong> on the module settings page (<em>Extensions > Modules > <?php echo $ext_name; ?></em>) by changing <em>Extension status</em> setting to "Disabled".</li>

														<li>
															<p><strong>Upload</strong> the <strong>extension archive</strong> <em>SimilarProducts-x.x.x.ocmod.zip</em> using the <a href="<?php echo $extension_installer; ?>" target="_blank">Extension Installer</a>.</p>
															<p><span class="label label-info">Note</span> Do not worry, no OpenCart core files will be replaced! Only the previously installed <?php echo $ext_name; ?> files will be overwritten.</p>
															<p><span class="label label-danger">Important</span> If you have done custom modifications to the extension (for example customized it for your theme) then back up the modified files and re-apply the modifications after upgrade. To see which files have changed, please take a look at the <a href="#" class="external-tab-link" data-target="#changelog,#about-ext">Changelog</a>.</p>
														</li>

														<li>
															<p><strong>Navigagte to</strong> the <strong>Modifications page</strong> <small>(<em>Extensions > Modifications</em>)</small> and <strong>rebuild the modification cache</strong> by clicking on the 'Refresh' button.</p>
														</li>

														<li>
															<p><strong>Open</strong> the <?php echo $ext_name; ?> <strong>module settings page</strong> <small>(<em>Extensions > Modules > <?php echo $ext_name; ?></em>)</small> and <strong>refresh the page</strong> by pressing <em>Ctrl + F5</em> twice to force the browser to update the css changes.</p>
														</li>

														<li><p>You should see a notice stating that new version of extension files have been found. <strong>Upgrade the extension</strong> by clicking on the 'Upgrade' button.</p></li>

														<li>After the extension has been successfully upgraded <strong>enable the extension</strong> by changing <em>Extension status</em> setting to "Enabled".</li>
													</ol>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div class="tab-pane" id="services">
									<h3>Premium Services<button type="button" class="btn btn-default btn-sm pull-right" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_refresh; ?>" id="btn-refresh-services" data-loading-text="<i class='fa fa-refresh fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_loading; ?></span>"><i class="fa fa-refresh"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_refresh; ?></span></button></h3>
									<div id="service-container">
										<p data-bind="visible: service_list_loading()">Loading service list ... <i class="fa fa-refresh fa-spin"></i></p>
										<p data-bind="visible: service_list_loaded() && services().length == 0">There are currently no available services for this extension.</p>
										<table class="table table-hover">
											<tbody data-bind="foreach: services">
												<tr class="srvc">
													<td>
														<h4 class="service" data-bind="html: name"></h4>
														<span class="help-block">
															<p class="description" data-bind="visible: description != '', html: description"></p>
															<p data-bind="visible: turnaround != ''"><strong>Turnaround time</strong>: <span class="turnaround" data-bind="html: turnaround"></span></p>
															<span class="hidden code" data-bind="html: code"></span>
														</span>
													</td>
													<td class="nowrap text-right top-pad"><span class="currency" data-bind="html: currency"></span> <span class="price" data-bind="html: price"></span></td>
													<td class="text-right"><button type="button" class="btn btn-sm btn-primary purchase"><i class="fa fa-shopping-cart"></i> Buy Now</button></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="about-ext">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#about-navbar-collapse">
									<span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<h3 class="panel-title"><i class="fa fa-info fa-fw"></i> <?php echo $tab_about; ?></h3>
							</div>
							<div class="collapse navbar-collapse" id="about-navbar-collapse">
								<ul class="nav navbar-nav">
									<li class="active"><a href="#ext_info" data-toggle="tab"><?php echo $tab_extension; ?></a></li>
									<li><a href="#changelog" data-toggle="tab"><?php echo $tab_changelog; ?></a></li>
								</ul>
							</div>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane active" id="ext_info">
									<div class="row">
										<div class="col-sm-12">
											<h3><?php echo $text_extension_information; ?></h3>

											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"><?php echo $entry_extension_name; ?></label>
												<div class="col-sm-9 col-md-10">
													<p class="form-control-static"><?php echo $ext_name; ?></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"><?php echo $entry_installed_version; ?></label>
												<div class="col-sm-9 col-md-10">
													<p class="form-control-static"><strong><?php echo $installed_version; ?></strong></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"><?php echo $entry_extension_compatibility; ?></label>
												<div class="col-sm-9 col-md-10">
													<p class="form-control-static"><?php echo $ext_compatibility; ?></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"><?php echo $entry_extension_store_url; ?></label>
												<div class="col-sm-9 col-md-10">
													<p class="form-control-static"><a href="<?php echo $ext_store_url; ?>" target="_blank"><?php echo htmlspecialchars($ext_store_url); ?></a></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"><?php echo $entry_copyright_notice; ?></label>
												<div class="col-sm-9 col-md-10">
													<p class="form-control-static">&copy; 2011 - <?php echo date("Y"); ?> Romi Agar</p>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10">
													<p class="form-control-static"><a href="#legal_text" id="legal_notice" data-toggle="modal"><?php echo $text_terms; ?></a></p>
												</div>
											</div>

											<h3 class="page-header"><?php echo $text_license; ?></h3>
											<p><?php echo $text_license_text; ?></p>

											<div class="form-group has-success">
												<label class="col-sm-3 col-md-2 control-label"><?php echo $entry_active_on; ?></label>
												<div class="col-sm-9 col-md-10">
													<p class="form-control-static" data-bind="text: asn"></p>
												</div>
											</div>
											<div class="form-group has-error" data-bind="visible: iasn() != ''">
												<label class="col-sm-3 col-md-2 control-label"><?php echo $entry_inactive_on; ?></label>
												<div class="col-sm-9 col-md-10">
													<p class="form-control-static" data-bind="text: iasn"></p>
												</div>
												<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 error-container">
													<span class="help-block error-text"><?php echo $help_purchase_additional_licenses; ?></span>
													<a href="<?php echo $ext_purchase_url; ?>" class="btn btn-sm btn-danger" target="_blank"><i class="fa fa-shopping-cart"></i> <?php echo $button_purchase_license; ?></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="changelog">
									<div class="row">
										<div class="col-sm-12">
											<div class="release">
												<h3>Version 4.1.7 <small class="release-date text-muted">09 Feb 2017</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-info">Changed:</em> Improved the display of multiple modules on a single page</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/extension/module/similar_products.php</li>
														<li>admin/view/template/extension/module/similar_products.tpl</li>
														<li>catalog/controller/extension/module/similar_products.php</li>
														<li>catalog/view/javascript/sp/custom.min.js</li>
														<li>catalog/view/theme/default/template/extension/module/similar_products.tpl</li>
														<li>system/similar_products.ocmod.xml</li>
														<li>system/similar_products_default_theme_patch.ocmod.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.1.6 <small class="release-date text-muted">27 Jul 2016</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Support for OpenCart 2.3.0.0</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>system/similar_products.ocmod.xml</li>
														<li>system/similar_products_default_theme_patch.ocmod.xml</li>
													</ul>

													<h4><i class="fa fa-plus text-success"></i> Files added:</h4>

													<ul>
														<li>admin/controller/extension/module/similar_products.php</li>
														<li>admin/language/en-gb/extension/module/similar_products.php</li>
														<li>admin/model/extension/module/similar_products.php</li>
														<li>admin/view/template/extension/module/similar_products*.tpl</li>
														<li>catalog/controller/extension/module/similar_products.php</li>
														<li>catalog/language/en-gb/extension/module/similar_products.php</li>
														<li>catalog/model/extension/module/similar_products.php</li>
														<li>catalog/view/theme/default/template/extension/module/similar_products*.tpl</li>
													</ul>

													<h4><i class="fa fa-minus text-danger"></i> Files removed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/en-gb/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/model/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products*.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>catalog/language/en-gb/module/similar_products.php</li>
														<li>catalog/language/english/module/similar_products.php</li>
														<li>catalog/model/module/similar_products.php</li>
														<li>catalog/view/theme/default/template/module/similar_products*.tpl</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.1.5 <small class="release-date text-muted">24 Apr 2016</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Undefined method error on product edit page</li>
														<li><em class="text-success">Fixed:</em> Minor UI bugs</li>
														<li><em class="text-info">Changed:</em> Upgraded Waypoints library</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/model/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>catalog/view/javascript/sp/custom.min.js</li>
														<li>catalog/view/theme/default/template/module/similar_products.tpl</li>
														<li>system/similar_products.ocmod.xml</li>
														<li>system/similar_products_default_theme_patch.ocmod.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.1.4 <small class="release-date text-muted">05 Mar 2016</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Support for OpenCart 2.2.0.0</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/model/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>admin/view/template/module/similar_products_module.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>catalog/model/module/similar_products.php</li>
														<li>system/similar_products.ocmod.xml</li>
														<li>system/similar_products_default_theme_patch.ocmod.xml</li>
													</ul>

													<h4><i class="fa fa-plus text-success"></i> Files added:</h4>

													<ul>
														<li>admin/language/en-gb/module/similar_products.php</li>
														<li>catalog/language/en-gb/module/similar_products.php</li>
													</ul>

												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.1.3 <small class="release-date text-muted">06 Dec 2015</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Applying Similar Products settings to all products</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>system/similar_products.ocmod.xml</li>
														<li>system/similar_products_default_theme_patch.ocmod.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.1.2 <small class="release-date text-muted">01 Sep 2015</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Module displayed in one of the column positions reserves unnecessary space if empty</li>
														<li><em class="text-success">Fixed:</em> Total count when limit was set to 0</li>
														<li><em class="text-success">Fixed:</em> Loading icon centering on Safari</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/view/javascript/sp/custom.min.js</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>catalog/model/module/similar_products.php</li>
														<li>catalog/view/javascript/sp/custom.min.js</li>
														<li>catalog/view/theme/default/template/module/similar_products.tpl</li>
														<li>system/helper/sp.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>system/similar_products.ocmod.xml</li>
														<li>system/similar_products_default_theme_patch.ocmod.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.1.1 <small class="release-date text-muted">12 Aug 2015</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Related products deletion</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>system/similar_products.ocmod.xml</li>
														<li>system/similar_products_default_theme_patch.ocmod.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.1.0 <small class="release-date text-muted">02 Jul 2015</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Fully OCMOD compatible</li>
														<li><em class="text-primary">New:</em> Options to select similar products by the same Manufacturer or by Manufacturer &amp; Category</li>
														<li><em class="text-primary">New:</em> Option to mass-apply changes to all products by the selected Manufacturer</li>
														<li><em class="text-info">Changed:</em> Minor UI fixes</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/model/module/similar_products.php</li>
														<li>admin/view/javascript/sp/custom.min.js</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>admin/view/template/module/similar_products_module.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>catalog/model/module/similar_products.php</li>
														<li>system/helper/sp.php</li>
													</ul>

													<h4><i class="fa fa-plus text-success"></i> Files added:</h4>

													<ul>
														<li>admin/view/stylesheet/sp/custom.min.css</li>
														<li>system/similar_products.ocmod.xml</li>
														<li>system/similar_products_default_theme_patch.ocmod.xml</li>
													</ul>

													<h4><i class="fa fa-minus text-danger"></i> Files removed:</h4>

													<ul>
														<li>admin/view/stylesheet/sp/css/custom.min.css</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.0.5 <small class="release-date text-muted">24 Apr 2015</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> PHP notices when module is initialized without parameters</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.0.4 <small class="release-date text-muted">24 Jan 2015</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Undefined index 'status' notice in store front</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/view/stylesheet/sp/css/custom.min.css</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.0.3 <small class="release-date text-muted">21 Dec 2014</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Similar products autoselect if category was present</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/view/javascript/sp/custom.min.js</li>
														<li>admin/view/stylesheet/sp/css/custom.min.css</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>system/helper/sp.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>

													<h4><i class="fa fa-plus text-success"></i> Files added:</h4>

													<ul>
														<li>admin/view/template/module/similar_products_module.tpl</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.0.2 <small class="release-date text-muted">31 Aug 2014</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Extension status on modules page</li>
														<li><em class="text-info">Changed:</em> Improved lazy initialization</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/model/module/similar_products.php</li>
														<li>catalog/language/english/module/similar_products.php</li>
														<li>catalog/view/theme/default/template/module/similar_products.tpl</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>

													<h4><i class="fa fa-minus text-danger"></i> Files removed:</h4>

													<ul>
														<li>catalog/view/theme/default/image/loading_similar.gif</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.0.1 <small class="release-date text-muted">09 May 2014</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Product Name sort order SQL error</li>
														<li><em class="text-success">Fixed:</em> Auto select by Custom String in Product Model SQL error</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/view/stylesheet/sp/css/custom.min.css</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/model/module/similar_products.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 4.0.0 <small class="release-date text-muted">05 Mar 2014</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Revamped admin interface</li>
														<li><em class="text-primary">New:</em> Date Added and Date Modified sort options</li>
														<li><em class="text-primary">New:</em> Autoselect by product tags, product model fragment or custom string in name or model</li>
														<li><em class="text-primary">New:</em> Multiple 'product/product' layouts supported</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/view/static/bull5i_sp_extension_terms.htm</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>catalog/view/theme/default/template/module/similar_products.tpl</li>
														<li>catalog/view/theme/default/template/module/similar_products_products.tpl</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>

													<h4><i class="fa fa-plus text-success"></i> Files added:</h4>

													<ul>
														<li>admin/model/module/similar_products.php</li>
														<li>admin/view/javascript/sp/custom.min.js</li>
														<li>admin/view/stylesheet/sp/*</li>
														<li>catalog/model/module/similar_products.php</li>
														<li>catalog/view/javascript/sp/custom.min.js</li>
														<li>system/helper/sp.php</li>
													</ul>

													<h4><i class="fa fa-minus text-danger"></i> Files removed:</h4>

													<ul>
														<li>admin/view/image/spm/extension_logo.png</li>
														<li>admin/view/static/bull5i_sp_extension_help.htm</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 3.2.2 <small class="release-date text-muted">06 Apr 2013</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Automatic 'product/product' route layout detection</li>
														<li><em class="text-info">Changed:</em> Minor page load improvements</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>catalog/view/theme/default/template/module/similar_products.tpl</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 3.2.1 <small class="release-date text-muted">12 Jan 2013</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Error displaying</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 3.2.0 <small class="release-date text-muted">27 Jan 2013</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-info">Changed:</em> Improved module height resizing on pagination</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/view/theme/default/template/module/similar_products.tpl</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>

													<h4><i class="fa fa-plus text-success"></i> Files added:</h4>

													<ul>
														<li>admin/view/static/bull5i_sp_extension_help.htm</li>
														<li>admin/view/static/bull5i_sp_extension_terms.htm</li>
													</ul>

													<h4><i class="fa fa-minus text-danger"></i> Files removed:</h4>

													<ul>
														<li>admin/view/static/rmg_extension_help.htm</li>
														<li>admin/view/static/rmg_extension_terms.htm</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 3.1.2 <small class="release-date text-muted">30 Oct 2013</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Compatibility with AQE &amp; AQE PRO extensions</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 3.1.1 <small class="release-date text-muted">27 Sep 2012</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> SQL error that appeared with negative limit value</li>
														<li><em class="text-success">Fixed:</em> Incorrect Similar Product count in certain cases</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 3.1.0 <small class="release-date text-muted">18 May 2012</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Option to automatically select similar products based on beginning fragment of product name</li>
														<li><em class="text-success">Fixed:</em> A module display issue</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>catalog/view/theme/default/template/module/similar_products.tpl</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 3.0.0 <small class="release-date text-muted">26 Mar 2012</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Option to sort by product quantity</li>
														<li><em class="text-primary">New:</em> Lazy loading</li>
														<li><em class="text-primary">New:</em> Pagination</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>catalog/language/english/module/similar_products.php</li>
														<li>catalog/view/theme/default/template/module/similar_products.tpl</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>

													<h4><i class="fa fa-plus text-success"></i> Files added:</h4>

													<ul>
														<li>catalog/view/theme/default/image/loading_similar.gif</li>
														<li>catalog/view/theme/default/template/module/similar_products_products.tpl</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 2.2.0 <small class="release-date text-muted">14 Feb 2012</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Option to display only products currently in stock</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>admin/language/english/module/similar_products.php</li>
														<li>admin/view/template/module/similar_products.tpl</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>

													<h4><i class="fa fa-plus text-success"></i> Files added:</h4>

													<ul>
														<li>admin/view/view/image/spm/extension_logo.png</li>
														<li>admin/view/view/static/rmg_extension_help.htm</li>
														<li>admin/view/view/static/rmg_extension_terms.htm</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 2.1.3 <small class="release-date text-muted">18 Jan 2012</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Errors due to which the module would not show up</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 2.1.2 <small class="release-date text-muted">11 Jan 2012</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Issue where leaf categories were not found and thus no similar products were shown</li>
													</ul>

													<h4><i class="fa fa-pencil text-primary"></i> Files changed:</h4>

													<ul>
														<li>admin/controller/module/similar_products.php</li>
														<li>catalog/controller/module/similar_products.php</li>
														<li>vqmod/xml/similar_products.xml</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 2.1.1 <small class="release-date text-muted">29 Dec 2011</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> Issue with product copy</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 2.1.0 <small class="release-date text-muted">21 Dec 2011</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Option to choose products only from leaf categories</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 2.0.0 <small class="release-date text-muted">16 Dec 2011</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Option to automatically select similar products from the current category</li>
														<li><em class="text-primary">New:</em> Option to sort similar products by name, model, sort order, most viewed or random order</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 1.1.1 <small class="release-date text-muted">31 Oct 2011</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-success">Fixed:</em> An undefined function call during extension installation</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 1.1.0 <small class="release-date text-muted">06 Oct 2011</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li><em class="text-primary">New:</em> Module positions - Content Top, Content Bottom, Column Left, Column Right</li>
														<li><em class="text-primary">New:</em> Adjustable image size</li>
													</ul>
												</blockquote>
											</div>

											<div class="release">
												<h3>Version 1.0.0 <small class="release-date text-muted">05 Oct 2011</small></h3>

												<blockquote>
													<ul class="list-unstyled">
														<li>Initial release</li>
													</ul>
												</blockquote>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript"><!--
!function(e,t,o){var s,a={module_id:"<?php echo addslashes($sp_m_module_id); ?>",names:[],show_in_tab:parseInt("<?php echo (int)$sp_m_show_in_tab; ?>"),products_per_page:parseInt("<?php echo (int)$sp_m_products_per_page; ?>"),limit:parseInt("<?php echo (int)$sp_m_limit; ?>"),image_width:parseInt("<?php echo (int)$default_image_width; ?>"),image_height:parseInt("<?php echo (int)$default_image_height; ?>"),stock_only:parseInt("<?php echo (int)$sp_m_stock_only; ?>"),lazy_load:parseInt("<?php echo (int)$sp_m_lazy_load; ?>"),status:parseInt("<?php echo (int)$sp_m_status; ?>")},r=<?php echo json_encode($errors); ?>,i=<?php echo json_encode($modules); ?>,n=<?php echo json_encode($languages); ?>,h=<?php echo json_encode(isset($sp_apply_to["selected"]) ? (array)$sp_apply_to["selected"] : array()); ?>,p=<?php echo json_encode($stores); ?>;e.texts=t.extend({},e.texts,{error_ajax_request:"<?php echo addslashes($error_ajax_request); ?>",error_module_name:"<?php echo addslashes($error_module_name); ?>",error_positive_integer:"<?php echo addslashes($error_positive_integer); ?>",default_name:"<?php echo addslashes($text_default_module_name); ?>"});var c=new Bloodhound({remote:'<?php echo str_replace("%TYPE%", "product", $autocomplete); ?>',datumTokenizer:Bloodhound.tokenizers.obj.whitespace("value"),queryTokenizer:Bloodhound.tokenizers.whitespace,dupDetector:function(e,t){return e.id&&t.id&&e.id==t.id},limit:10});c.initialize(),e.load_service_list=function(e){var e=e!==o?1*e:0,a=t.Deferred();return s.service_list_loaded()&&!e||s.service_list_loading()?a.reject():(s.service_list_loading(!0),t.when(t.ajax({url:"<?php echo $services; ?>",data:{force:e},dataType:"json"})).then(function(e){s.service_list_loaded(!0),s.service_list_loading(!1),s.clearServices(),e.services&&t.each(e.services,function(e,t){var o=t.code,a=t.name,r=t.description||"",i=t.currency,n=t.price,h=t.turnaround;s.addService(o,a,r,i,n,h)}),e.rate&&t("#hourly_rate").html(e.rate),a.resolve()},function(e,t,o){s.service_list_loaded(!0),s.service_list_loading(!1),a.reject(),window.console&&window.console.log&&window.console.log("Failed to load services list: "+o)})),a.promise()};var l=function(e){isNaN(parseInt(e))||this.hasOwnProperty("minValue")&&parseInt(e)<this.minValue||this.hasOwnProperty("maxValue")&&parseInt(e)>this.maxValue?(this.target.hasError(!0),this.target.errorMsg(this.message)):(this.target.hasError(!1),this.target.errorMsg(""))},d=function(e,t,o,s,a,r){this.code=e,this.name=t,this.description=o,this.currency=s,this.price=a,this.turnaround=r},_=function(e,t,o){this.id=e,this.name=t,this.flag=o},u=function(e,t,o){this.id=e,this.name=t,this.model=o},m=function(t,o){var s=this;this.language_id=ko.observable(t),this.name=ko.observable(o).extend({required:{message:e.texts.error_module_name,context:s}}),this.hasError=ko.computed(this.hasError,this)};m.prototype=new e.observable_object_methods;var v=function(o,s,a,r,i,n,h,p,c,d,_){var u=this,v={};t.each(_.languages,function(t,o){v[o.id]=s.hasOwnProperty(o.id)?s[o.id]:e.texts.default_name}),this.module_id=ko.observable(o),this.names=ko.observableArray(t.map(v,function(e,t){return new m(t,e,u)})).withIndex("language_id").extend({hasError:{check:!0,context:u},applyErrors:{context:u},updateValues:{context:u}}),this.name=ko.computed(function(){return u.names.findByKey("<?php echo $default_language; ?>").name()}),this.show_in_tab=ko.observable(a),this.limit=ko.observable(r).extend({numeric:{precision:0,context:u}}),this.image_width=ko.observable(i).extend({numeric:{precision:0,context:u},validate:{message:e.texts.error_positive_integer,context:u,method:l,minValue:0}}),this.image_height=ko.observable(n).extend({numeric:{precision:0,context:u},validate:{message:e.texts.error_positive_integer,context:u,method:l,minValue:0}}),this.products_per_page=ko.observable(h).extend({numeric:{precision:0,context:u},validate:{message:e.texts.error_positive_integer,context:u,method:l,minValue:0}}),this.status=ko.observable(p),this.stock_only=ko.observable(c),this.lazy_load=ko.observable(d),this.hasError=ko.computed(this.hasError,this)};v.prototype=new e.observable_object_methods;var g=function(){var e=this;this.languages={},t.each(n,function(t,o){e.languages[t]=new _(o.language_id,o.name,(o.hasOwnProperty("image")&&o.image)?"view/image/flags/"+o.image:"language/"+o.code+"/"+o.code+".png")}),this.default_language="<?php echo $default_language; ?>",this.status=ko.observable("<?php echo $sp_status; ?>"),this.remove_sql_changes=ko.observable("<?php echo $sp_remove_sql_changes; ?>"),this._sas=ko.observable(0),this._as=ko.observableArray(JSON.parse(atob("<?php echo $sp_as; ?>"))),this.as=ko.computed(function(){return btoa(JSON.stringify(e._as()))}),this.asn=ko.computed(function(){var t=[];return ko.utils.arrayForEach(e._as(),function(e){p.hasOwnProperty(e)&&t.push(p[e].name)}),t.join(", ")}),this.iasn=ko.computed(function(){var o=[];return t.map(p,function(t,s){-1==e._as().indexOf(s.toString())&&o.push(t.name)}),o.join(", ")}),this.general_errors=ko.computed(function(){return!1}),this.auto_select=ko.observable("<?php echo $sp_auto_select; ?>"),this.product_sort_order=ko.observable("<?php echo $sp_product_sort_order; ?>"),this.leaves_only=ko.observable("<?php echo $sp_leaves_only; ?>"),this.substr_start=ko.observable("<?php echo $sp_substr_start; ?>"),this.substr_length=ko.observable("<?php echo $sp_substr_length; ?>"),this.custom_string=ko.observable("<?php echo $sp_custom_string; ?>"),this.products=ko.observable('<?php echo isset($sp_apply_to["products"]) ? $sp_apply_to["products"] : ""; ?>'),this.category=ko.observable('<?php echo isset($sp_apply_to["category"]) ? $sp_apply_to["category"] : ""; ?>'),this.manufacturer=ko.observable('<?php echo isset($sp_apply_to["manufacturer"]) ? $sp_apply_to["manufacturer"] : ""; ?>'),this.selected=ko.observableArray(ko.utils.arrayMap(h,function(e){return new u(e.product_id,e.name,e.model)})),this.modules=ko.observableArray(t.map(i,function(t){return new v(t.hasOwnProperty("module_id")?t.module_id:a.module_id,t.hasOwnProperty("names")?t.names:a.names,t.hasOwnProperty("show_in_tab")?t.show_in_tab:a.show_in_tab,t.hasOwnProperty("limit")?t.limit:a.limit,t.hasOwnProperty("image_width")?t.image_width:a.image_width,t.hasOwnProperty("image_height")?t.image_height:a.image_height,t.hasOwnProperty("products_per_page")?t.products_per_page:a.products_per_page,t.hasOwnProperty("status")?t.status:a.status,t.hasOwnProperty("stock_only")?t.stock_only:a.stock_only,t.hasOwnProperty("lazy_load")?t.lazy_load:a.lazy_load,e)})).extend({hasError:{check:!0,context:e},applyErrors:{context:e},updateValues:{context:e}}),this.module_errors=ko.computed(function(){return e.modules.hasError()}),this.tabPositionUsed=ko.computed(function(){var t=!1;return ko.utils.arrayForEach(e.modules(),function(e){"1"==e.show_in_tab()&&(t=!0)}),t}),e.service_list_loaded=ko.observable(!1),e.service_list_loading=ko.observable(!1),e.services=ko.observableArray([]),this.ta_dataset={name:"product",source:c.ttAdapter(),templates:{empty:['<div class="tt-no-suggestion"><?php echo addslashes($text_no_records_found); ?></div>'].join("\n"),suggestion:Handlebars.compile(['<p><span class="tt-nowrap">{{value}}<span class="tt-secondary-right">{{model}}</span></span></p>'].join(""))}},this.addService=function(t,o,s,a,r,i){e.services.push(new d(t,o,s,a,r,i))},e.clearServices=function(){e.services.removeAll()},this.addModule=function(){e.modules.push(new v("",[],a.show_in_tab,a.limit,a.image_width,a.image_height,a.products_per_page,a.status,a.stock_only,a.lazy_load,e))},this.deleteModule=function(t){t&&e.modules.remove(t)},this.removeSelected=function(t){e.selected.remove(t)},this.addSelected=function(o,s,a){var r=!1;"3"==e.products()&&(t.each(e.selected(),function(e,t){return t.id==o?void(r=!0):void 0}),r||e.selected.push(new u(o,s,a)))},this.checkErrors=function(){for(var t in e)ko.isObservable(e[t])&&"function"==typeof e[t].checkErrors&&e[t].checkErrors()}};g.prototype=new e.observable_object_methods,t(function(){var o=window.location.hash,a=o.split("?")[0];s=e.view_model=new g,e.view_models=t.extend({},e.view_models,{ExtVM:e.view_model}),s.checkErrors(),s.applyErrors(r),ko.applyBindings(s,t("#content")[0]),t("#legal_text .modal-body").load("view/static/bull5i_sp_extension_terms.htm"),t("body").on("shown.bs.tab","a[data-toggle='tab'][href='#ext-support'],a[data-toggle='tab'][href='#services']",function(){e.load_service_list()}).on("keydown","#sp_status",function(e){if(e.altKey&&e.shiftKey&&e.ctrlKey&&83==e.keyCode){var t=ko.dataFor(this);t._sas(0==t._sas()?1:0)}}).on("typeahead:selected",".typeahead",function(e,o){var s=(ko.contextFor(this),ko.dataFor(this)),a=t(this).data("method");a&&(s[a](o.id,o.value,o.model),t(this).typeahead("val",""))}),e.onComplete(t("#page-overlay"),t("#content")).done(function(){e.activateTab(a)})})}(window.bull5i=window.bull5i||{},jQuery);
//--></script>
<?php echo $footer; ?>
