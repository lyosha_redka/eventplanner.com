<?php echo $header; ?>
<!-- confirm deletion -->
<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="confirmDeleteLabel"><?php echo $text_confirm_delete; ?></h4>
			</div>
			<div class="modal-body">
				<p><?php echo $text_are_you_sure; ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> <?php echo $button_cancel; ?></button>
				<button type="button" class="btn btn-danger delete"><i class="fa fa-trash-o"></i> <?php echo $button_delete; ?></button>
			</div>
		</div>
	</div>
</div>
<?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<ul class="breadcrumb bull5i-breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li<?php echo ($breadcrumb['active']) ? ' class="active"' : ''; ?>><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
			<div class="navbar-placeholder">
				<nav class="navbar navbar-bull5i" role="navigation" id="bull5i-navbar">
					<div class="nav-container">

						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bull5i-navbar-collapse">
								<span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<h1 class="bull5i-navbar-brand"><i class="fa fa-files-o fa-fw ext-icon"></i> <?php echo $heading_title; ?></h1>
						</div>
						<div class="collapse navbar-collapse" id="bull5i-navbar-collapse">
							<div class="nav navbar-nav btn-group navbar-right">
								<button type="button" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_apply; ?>" class="btn btn-success" id="btn-apply" data-url="<?php echo $save; ?>" data-form="#sForm" data-context="#content" data-vm="ModuleVM" data-overlay="#page-overlay" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_saving; ?></span>"><i class="fa fa-check"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_apply; ?></span></button>
								<button type="submit" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_save; ?>" class="btn btn-primary" id="btn-save" data-url="<?php echo $save; ?>" data-form="#sForm" data-context="#content" data-overlay="#page-overlay" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_saving; ?></span>"><i class="fa fa-save"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_save; ?></span></button>
								<button type="button" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_delete; ?>" class="btn btn-danger" id="btn-delete" data-url="<?php echo $delete; ?>" data-form="#sForm" data-context="#content" data-overlay="#page-overlay" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_deleting; ?></span>"><i class="fa fa-trash-o"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_delete; ?></span></button>
								<a href="<?php echo $cancel; ?>" class="btn btn-default" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_cancel; ?>" id="btn-cancel" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_canceling; ?></span>"><i class="fa fa-ban"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_cancel; ?></span></a>
								<a href="<?php echo $general_settings; ?>" class="btn btn-default btn-nav-link" data-toggle="tooltip" data-container="body" data-placement="bottom" title="<?php echo $button_general_settings; ?>" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span class='visible-lg-inline visible-xs-inline'><?php echo $text_opening; ?></span>"><i class="fa fa-cog"></i> <span class="visible-lg-inline visible-xs-inline"><?php echo $button_general_settings; ?></span></a>
							</div>
						</div>

					</div>
				</nav>
			</div>
		</div>
	</div>

	<div class="alerts">
		<div class="container-fluid" id="alerts">
			<?php foreach ($alerts as $_type => $_alerts) { ?>
				<?php foreach ((array)$_alerts as $alert) { ?>
					<?php if ($alert) { ?>
			<div class="alert alert-<?php echo ($_type == "error") ? "danger" : $_type; ?> fade in">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="fa <?php echo $alert_icon($_type); ?>"></i><?php echo $alert; ?>
			</div>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</div>
	</div>

	<div class="container-fluid bull5i-content bull5i-container">
		<div id="page-overlay" class="bull5i-overlay fade in">
			<div class="page-overlay-progress"><i class="fa fa-refresh fa-spin fa-5x text-muted"></i></div>
		</div>

		<form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="sForm" class="form-horizontal" role="form">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-pencil fa-fw"></i> <?php echo $text_edit_module; ?></h3></div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="alert alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="fa fa-info-circle"></i> <?php echo $text_product_layout; ?>
							</div>
							<fieldset>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 control-label" data-bind="attr: {for: 'name<?php echo $default_language; ?>'}, css: {'has-error': names.hasError}"><?php echo $entry_name; ?></label>
									<!-- ko foreach: names -->
									<div class="col-sm-6 col-md-6 col-lg-5" data-bind="css: {'multi-row col-sm-offset-3 col-md-offset-2': $index() != 0, 'has-error': name.hasError}">
										<div class="input-group">
											<span class="input-group-addon" data-bind="attr: {title: $root.languages[language_id()].name}, tooltip: {title: $root.languages[language_id()].name}"><img data-bind="attr: {src: $root.languages[language_id()].flag, title: $root.languages[language_id()].name}" /></span>
											<input data-bind="attr: {name: 'names[' + language_id() + ']', id: 'name' + language_id()}, value: name" class="form-control">
										</div>
									</div>
									<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 has-error" data-bind="visible: name.hasError">
										<span class="help-block" data-bind="text: name.errorMsg"></span>
									</div>
									<!-- /ko -->
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 control-label" data-bind="attr: {for: 'show_in_tab'}"><?php echo $entry_show_in_tab; ?></label>
									<div class="col-sm-3 col-md-2 fc-auto-width">
										<select data-bind="attr: {name: 'show_in_tab', id: 'show_in_tab'}, value: show_in_tab" class="form-control">
											<option value="0"><?php echo $text_no; ?></option>
											<!-- ko if: !tab_position_used() || show_in_tab() == '1' --><option value="1"><?php echo $text_yes; ?></option><!-- /ko -->
										</select>
									</div>
									<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
										<span class="help-block help-text"><?php echo $help_show_in_tab; ?></span>
									</div>
								</div>
								<div class="form-group" data-bind="css: {'has-error': products_per_page.hasError}">
									<label class="col-sm-3 col-md-2 control-label" data-bind="attr: {for: 'per_page'}"><?php echo $entry_products_per_page; ?></label>
									<div class="col-sm-2 col-lg-1">
										<input data-bind="attr: {name: 'products_per_page', id: 'per_page'}, value: products_per_page" class="form-control text-right">
									</div>
									<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 has-error" data-bind="visible: products_per_page.hasError && products_per_page.errorMsg">
										<span class="help-block" data-bind="text: products_per_page.errorMsg"></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 control-label" data-bind="attr: {for: 'limit'}"><?php echo $entry_limit; ?></label>
									<div class="col-sm-2 col-lg-1">
										<input data-bind="attr: {name: 'limit', id: 'limit'}, value: limit" class="form-control text-right">
									</div>
								</div>
								<div class="form-group" data-bind="css: {'has-error': image_width.hasError}">
									<label class="col-sm-3 col-md-2 control-label" data-bind="attr: {for: 'image_width'}"><?php echo $entry_image_width; ?></label>
									<div class="col-sm-2 col-lg-1">
										<input data-bind="attr: {name: 'image_width', id: 'image_width'}, value: image_width" class="form-control text-right">
									</div>
									<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 has-error" data-bind="visible: image_width.hasError && image_width.errorMsg">
										<span class="help-block" data-bind="text: image_width.errorMsg"></span>
									</div>
								</div>
								<div class="form-group" data-bind="css: {'has-error': image_height.hasError}">
									<label class="col-sm-3 col-md-2 control-label" data-bind="attr: {for: 'image_height'}"><?php echo $entry_image_height; ?></label>
									<div class="col-sm-2 col-lg-1">
										<input data-bind="attr: {name: 'image_height', id: 'image_height'}, value: image_height" class="form-control text-right">
									</div>
									<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 has-error" data-bind="visible: image_height.hasError && image_height.errorMsg">
										<span class="help-block" data-bind="text: image_height.errorMsg"></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 control-label" data-bind="attr: {for: 'stock_only0'}"><?php echo $entry_stock_only; ?></label>
									<div class="col-sm-9 col-md-10">
										<label class="radio-inline">
											<input type="radio" name="stock_only" id="stock_only1" value="1" data-bind="checked: stock_only"> <?php echo $text_yes; ?>
										</label>
										<label class="radio-inline">
											<input type="radio" name="stock_only" id="stock_only0" value="0" data-bind="checked: stock_only"> <?php echo $text_no; ?>
										</label>
									</div>
									<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
										<span class="help-block help-text"><?php echo $help_stock_only; ?></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 control-label" data-bind="attr: {for: 'lazy_load'}"><?php echo $entry_lazy_load; ?></label>
									<div class="col-sm-9 col-md-10">
										<label class="radio-inline">
											<input type="radio" name="lazy_load" id="lazy_load1" value="1" data-bind="checked: lazy_load"> <?php echo $text_yes; ?>
										</label>
										<label class="radio-inline">
											<input type="radio" name="lazy_load" id="lazy_load0" value="0" data-bind="checked: lazy_load"> <?php echo $text_no; ?>
										</label>
									</div>
									<div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 help-container">
										<span class="help-block help-text"><?php echo $help_lazy_load; ?></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 control-label" for="status"><?php echo $entry_status; ?></label>
									<div class="col-sm-2 fc-auto-width">
										<select name="status" id="status" data-bind="value: status" class="form-control">
											<option value="1"><?php echo $text_enabled; ?></option>
											<option value="0"><?php echo $text_disabled; ?></option>
										</select>
										<input type="hidden" data-bind="attr: {name: 'name'}, value: name() + (show_in_tab() == '1' ? ' (Tab)' : '')">
										<input type="hidden" data-bind="attr: {name: 'module_id'}, value: module_id">
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript"><!--
!function(e,t){var o,s=<?php echo json_encode($errors); ?>,a=<?php echo json_encode($languages); ?>,r=<?php echo json_encode($names); ?>;e.texts=t.extend({},e.texts,{error_ajax_request:"<?php echo addslashes($error_ajax_request); ?>",error_module_name:"<?php echo addslashes($error_module_name); ?>",error_positive_integer:"<?php echo addslashes($error_positive_integer); ?>",default_name:"<?php echo addslashes($text_default_module_name); ?>"});var i=function(e){isNaN(parseInt(e))||parseInt(e)<0?(this.target.hasError(!0),this.target.errorMsg(this.message)):(this.target.hasError(!1),this.target.errorMsg(""))},n=function(e,t,o){this.id=e,this.name=t,this.flag=o},h=function(t,o){var s=this;this.language_id=ko.observable(t),this.name=ko.observable(o).extend({required:{message:e.texts.error_module_name,context:s}}),this.hasError=ko.computed(this.hasError,this)};h.prototype=new e.observable_object_methods;var p=function(){var o=this,s={};this.languages={},t.each(a,function(t,a){o.languages[t]=new n(a.language_id,a.name,(a.hasOwnProperty("image")&&a.image)?"view/image/flags/"+a.image:"language/"+a.code+"/"+a.code+".png"),s[a.language_id]=r.hasOwnProperty(a.language_id)?r[a.language_id]:e.texts.default_name}),this.module_id=ko.observable("<?php echo $module_id; ?>"),this.names=ko.observableArray(t.map(s,function(e,t){return new h(t,e,o)})).withIndex("language_id").extend({hasError:{check:!0,context:o},applyErrors:{context:o},updateValues:{context:o}}),this.name=ko.computed(function(){return o.names.findByKey("<?php echo $default_language; ?>").name()}),this.show_in_tab=ko.observable("<?php echo $show_in_tab; ?>"),this.limit=ko.observable("<?php echo $limit; ?>").extend({numeric:{precision:0,context:o}}),this.image_width=ko.observable("<?php echo $image_width; ?>").extend({numeric:{precision:0,context:o},validate:{message:e.texts.error_positive_integer,context:o,method:i}}),this.image_height=ko.observable("<?php echo $image_height; ?>").extend({numeric:{precision:0,context:o},validate:{message:e.texts.error_positive_integer,context:o,method:i}}),this.products_per_page=ko.observable("<?php echo $products_per_page; ?>").extend({numeric:{precision:0,context:o},validate:{message:e.texts.error_positive_integer,context:o,method:i}}),this.status=ko.observable("<?php echo $status; ?>"),this.stock_only=ko.observable("<?php echo $stock_only; ?>"),this.lazy_load=ko.observable("<?php echo $lazy_load; ?>"),this.tab_position_used=ko.observable(parseInt("<?php echo $tab_position_used; ?>"))};p.prototype=new e.observable_object_methods,t(function(){o=e.view_model=new p,e.view_models=t.extend({},e.view_models,{ModuleVM:e.view_model}),o.applyErrors(s),ko.applyBindings(o,t("#content")[0]),e.onComplete(t("#page-overlay"),t("#content"))})}(window.bull5i=window.bull5i||{},jQuery);
//--></script>
<?php echo $footer; ?>
