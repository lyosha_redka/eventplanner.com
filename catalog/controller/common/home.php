<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');




		$this->load->model('extension/module');
		 $select_module = $this->model_extension_module->getModuleText('how_plan');
		 $data['how_plan'] = html_entity_decode($select_module['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');

		 $select_vendor = $this->model_extension_module->getModuleText('vendor_block');
		 $data['vendor_block'] = html_entity_decode($select_vendor['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');

		 $select_seo_home = $this->model_extension_module->getModuleText('seo_home_text');
		 $data['seo_home_text'] = html_entity_decode($select_seo_home['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');

		 $select_event_uslugi = $this->model_extension_module->getModuleText('event_uslugi_block');
		 $data['event_uslugi_block'] = html_entity_decode($select_event_uslugi['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		

		

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
