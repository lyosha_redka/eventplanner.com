<?php echo $header; ?>
<div class="container">
<!--   <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> -->
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="block-head-category">
        <h1 class="category-title"><?php echo $heading_title; ?></h1>
      </div>
      <?php if ($thumb || $description) { ?>
      
      <?php } ?>
      <?php if ($categories) { ?>

      <div class="row">

            <?php foreach ($categories as $category) { ?>
            <div class="product-layout product-list col-sm-3 col-xs-12">
              <div class="block-item-idea-category">
                <div class="flip-container">
                  <div class="idea-article-block">
                    <div class="product-article-idea transition post-item-idea front">
                      <?php if ($category['thumb']) { ?>
                      <div class="image-idea"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
                      <?php } ?>
                        <div class="caption-idea">
                          <div class="title-article-idea"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></div>
                        </div>
                      <!-- <div class="button-group-more idea-group-more">
                        <button onclick="location.href = ('<?php echo $article['href']; ?>');" data-toggle="tooltip" title="<?php echo $text_more; ?>" class="btn-idea"> <span class="more-text-idea"><?php echo $text_more; ?></span></button>
                      </div> -->
                    </div>

                    <div class="back">

                      <div class="caption-idea-back">
                        <div class="title-article-idea-back"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></div>
                      </div>

                      <div class="button-group-more idea-group-more">
                        <button onclick="location.href = ('<?php echo $category['href']; ?>');" class="btn-idea"> 
                          <span class="more-text-idea">Читать полностью</span>
                        </button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

      </div>

      <?php } ?>
      <?php if ($articles) { ?>
      <div class="row">
        <?php foreach ($articles as $article) { ?>
        <div class="product-layout product-list col-sm-3 col-xs-12">
          <div class="block-item-idea-category">
            <div class="flip-container">
            <div class="idea-article-block">
              <div class="product-article-idea transition post-item-idea front">
                <?php if ($article['thumb']) { ?>
                <div class="image-idea"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a></div>
                <?php } ?>
                  <div class="caption-idea">
                    <div class="title-article-idea"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
                    <div class="article-preview-idea"><?php echo $article['preview']; ?></div>
                  </div>
                <!-- <div class="button-group-more idea-group-more">
                  <button onclick="location.href = ('<?php echo $article['href']; ?>');" data-toggle="tooltip" title="<?php echo $text_more; ?>" class="btn-idea"> <span class="more-text-idea"><?php echo $text_more; ?></span></button>
                </div> -->
              </div>

              <div class="back">

                <div class="caption-idea-back">
                  <div class="title-article-idea-back"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
                  <div class="article-preview-idea-back"><?php echo $article['preview']; ?></div>
                </div>

                <div class="button-group-more idea-group-more">
                  <button onclick="location.href = ('<?php echo $article['href']; ?>');" class="btn-idea"> 
                    <span class="more-text-idea">Читать полностью</span>
                  </button>
                </div>

              </div>
            </div>
          </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      </div>


      


      <?php } ?>

      <div class="row">
        <?php if ($description) { ?>
        <div class="col-sm-12"><div class="block-text-category"><?php echo $description; ?></div></div>
        <?php } ?>
      </div>


      <?php if (!$categories && !$articles) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php if ($comments_vk) { ?>
      <div class="row">
        <div class="col-md-12">
			<div id="vk_comments"></div>
			<script type="text/javascript">
			VK.init({apiId: <?php echo $comments_vk; ?>, onlyWidgets: true});
			VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"});
			</script>
        </div>
      </div>
      <?php } ?>

      <?php if ($comments_fb) { ?>
      <div class="row">
        <div class="col-md-12">
            <div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-comments" data-href="<?php echo $canonical; ?>" data-width="100%" data-numposts="10"></div>
        </div>
      </div>
      <?php } ?>

      <?php if ($comments_dq) { ?>
      <div class="row">
        <div class="col-md-12">
        	<div id="disqus_thread"></div>
			<script>
			var disqus_config = function () {
				this.page.url = "<?php echo $canonical; ?>";
			};

			(function() { // DON'T EDIT BELOW THIS LINE
			var d = document, s = d.createElement('script');
			s.src = 'https://<?php echo $comments_dq; ?>.disqus.com/embed.js';
			s.setAttribute('data-timestamp', +new Date());
			(d.head || d.body).appendChild(s);
			})();
			</script>
			<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>