<div id="search" class="input-group">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg search-input" />
  <span class="input-group-btn">
    <button type="button" class="btn btn-default btn-lg btn-search"><img src="/image/catalog/serach.png"></button>
  </span>
</div>