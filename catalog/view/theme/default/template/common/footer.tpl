<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="item-footer-block">
          <img src="/image/catalog/static/home/logotype.png" class="footer-logotype">
          <div class="contact-information">
            <a href="mailto:info@eventplanner.pro" class="link-contact-mail">info@eventplanner.pro</a>

            <p class="row-tel-footer">
              <a href="tel:+380631777777" class="contact-tel-footer">+38 063 177 77 77</a>
              <a href="tel:+380631777777" class="contact-tel-footer">+38 063 177 77 77</a>
            </p>

            <p class="social-link-footer">
              <a href="#facebook"><img src="/image/catalog/static/footer/facebook.png"></a>
              <a href="#twitter"><img src="/image/catalog/static/footer/telegram.png"></a>
              <a href="#instagram"><img src="/image/catalog/static/footer/instagram.png"></a>
            </p>

            <p class="oplata">
              <img src="/image/catalog/static/footer/Visa_Inc._logo.svg" class="oplata-icon">
              <img src="/image/catalog/static/footer/mc_vrt_pos.svg" class="oplata-icon">
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="item-footer-block">
          <p  class="title-footer-block">Компания</p>
            <nav class="footer-menu-block">
              <a href="#">Карта сайта</a>
              <a href="/blog">Блог</a>
              <a href="#">Отправить в блог</a>
              <a href="#">Встретиться с командой</a>
              <a href="#">Политика конфиденциальности</a>
              <a href="#">Публичная оферта</a>
              <a href="#">Тарифы</a>
              <a href="#">Обратная связь</a>
            </nav>
        </div>
      </div>


      <div class="col-md-3 col-sm-6">
        <div class="item-footer-block">
          <p  class="title-footer-block">Категория</p>
            <nav class="footer-menu-block">
              <a href="/fotografiya/">Фото</a>
              <a href="/transport/">Транспорт</a>
              <a href="/keitering/">Еда</a>
              <a href="/personal/">Персонал</a>
              <a href="/razvlecheniya/">Развлечения</a>
              <a href="/ivent-uslugi" class="text-decoration-link">Смотреть все категории</a>
            </nav>
        </div>
      </div>


      <div class="col-md-3 col-sm-6">
        <div class="item-footer-block">
          <p  class="title-footer-block">Предложить услугу</p>
          <p class="desc-footer-block">Вы также можете предложить свои услуги и мы опубликуем их на своем ресурсе!</p>

          <a href="" class="btn-block-footer see-vendors popupUp-call">предложить услугу</a>
        </div>
      </div>


    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="flex-row-footer">
          <span class="copyright-text">© 2019 Event Planner</span>
          <span class="powered-text">Создано с любовью в <a href="https://voll.com.ua" target="_blank" class="voll">веб студии VOLL</a></span>
        </div>
      </div>
    </div>
  </div>
</footer>


<a id="button-top"><span class="arrow-up-to"><img src="/image/catalog/static/home/up.png" class="up-strilka"></span><span class="text-up">наверх</span></a>

<script type="text/javascript" src="/catalog/view/javascript/carousel-special.js"></script>


<script>



  
  $(document).ready(function() { 

  
  var showhide = $('.seohide'); // multiple

  var curText = 'Показать еще';
  var openText = 'Скрыть';

  showhide.each(function(){
    $(this).append('<div class="show-button">' + curText + '</div>');
  });

  var showButton = $('.show-button');

showButton.each(function(){
  $(this).click(function(){
    var hideContent = $(this).parent().children('.hide-content'); // perhapse
    hideContent.toggleClass("show");

    if($(this).text() == curText){
      $(this).text(openText);
    }
    else{
      $(this).text(curText);
    }
  });
});
  
});




  $(document).ready(function() { 
  var btn = $('#button-top');

  $(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
      btn.addClass('show');
    } else {
      btn.removeClass('show');
    }
  });

  btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
  });

});







  $(document).ready(function() { 
var x, i, j, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);

});






  $(document).ready(function() {

  $(window).scroll(function(){

  if($(window).scrollTop()>50){
  $('.main-header').addClass("fixedHeader");
  // $('.header-bg').css({'margin-top': '80px'});
  } 
  else {
  $('.main-header').removeClass("fixedHeader");
  // $('.header-bg').css({'margin-top': '0px'});
  }

  })
});


</script>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
</div>

</body>

</html>