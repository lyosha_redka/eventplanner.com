<?php echo $header; ?>
<div class="container2">







<section class="mainSlider-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="main-slider">Вы найдете все,<br />
что искали для <br />
вашего ивента</h1>
            </div>
        </div>

        <div class="row">
            <div class="form-search">
                <div class="col-sm-12"><p class="form-title-main-sect">давайте выберем, что вам нужно</p></div>
                <form action="/search" id="">
                    <div class="col-sm-3">
                        <div class="input-block-search-home custom-select">
                            <select name="city-select">
                                <option value="">Город</option>
                                <option value="Киев">Киев</option>
                                <option value="Донецк">Донецк</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="input-block-search-home custom-select">
                            <select name="merop-select">
                                <option value="">Что празднуем?</option>
                                <option value="День Рождения">День Рождения</option>
                                <option value="Свадьба">Свадьба</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="input-block-search-home custom-select">
                            <select name="idea-select">
                                <option value="">Идея ивент услуги</option>
                                <option value="Идея 1">Идея 1</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="input-block-search-home">
                            <button type="submit" class="submit-search"><img src="/image/white_arrow.png"</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<section class="how-plan-section">
    <div class="container">
        <?php echo $how_plan ?>
    </div>
</section>

<section class="section-vendor">
    <div class="container">
        <?php echo $vendor_block ?>
    </div>
</section>


<section class="idea-item-section">
    <div class="container block-idei-bg">

        <div class="row">
            <div class="col-sm-12">
                <div class="title-blog">Идеи для ваших ивентов</div>
            </div>
        </div>
        <div class="block-img-plyama">
            <img src="/image/catalog/static/home/plyama.jpg" class="plyama-img">
        </div>
        <?php echo $content_bottom; ?>
    </div>
</section>



<section class="blog-item-section">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="title-blog">Блог</div>
            </div>
        </div>

        <?php echo $content_top; ?>
    </div>
</section>





<section class="event-uslugi-section">
    <div class="container">
        <?php echo $event_uslugi_block ?>
    </div>
</section>


<section class="seotext-section">
    <div class="container">
        <?php echo $seo_home_text ?>
    </div>
</section>

</div>


<?php echo $footer; ?>