<div class="container">

<!--   <div class="row">
    <div class="col-sm-12">


      <div class="owl-carousel owl-theme special-carousel">
      <?php foreach ($products as $product) { ?>

        <div class="item">

          <div class="product-layout-special img-special">
            <div class="product-thumb-special transition">
              <div class="image">
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
              </div>
              
              <div class="caption">
                <div class="product-name-special"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                <p><?php echo $product['description']; ?></p>


                <?php if ($product['price']) { ?>
                  <p class="price">
                    <?php if (!$product['special']) { ?>
                    <?php echo $product['price']; ?>
                    <?php } else { ?>
                    <span class="price-new"><?php echo $product['special']; ?></span>
                    <?php } ?>
                  </p>
                <?php } ?>


              </div>
              <div class="button-group">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
              </div>

            </div>
          </div>

        </div>

      <?php } ?>
    </div>
  </div>
  </div> -->




<div class="row">
  <div class="col-sm-12">
    <div class="title-carousel-spec">Найдите скидки для своего ивента</div>

    <!-- <div class="block-carousel-spec">
      <div class="carousel">
        <div class="img" src="/image/catalog/special/img.jpg"><img id="img-block-carousel" src="/image/catalog/special/img.jpg" alt="Фотограф" /><span class="name-spec">Фотограф</span></div>
        <div class="img"><img id="img-block-carousel" src="/image/catalog/special/img_2.jpg" alt="Фотограф" /></div>
        <div class="img"><img id="img-block-carousel" src="/image/catalog/special/img.jpg" alt="Фотограф" /></div>
      </div>
    </div> -->

  <div class="carousel">
  <div class="carousel-item">
    <img src="/image/catalog/special/img.jpg" alt="Dog" title="Dog">
    <div class="block-name-desc"><span class="name-spec">Фотограф</span><span class="desc-spec">Андрей, занимаюсь фотографией</span></div>
    <div class="skidka-block"><span><span class="block-city-date">Киев</span><span class="date-date">20.02.2020</span></span><span class="skidka-spec">-35%</span></div>
  </div>
  
  <div class="carousel-item">
    <img src="/image/catalog/special/img_2.jpg" alt="Dog" title="Dog">
    <div class="block-name-desc"><span class="name-spec">Повар</span><span class="desc-spec">Приготовлю для вас блюдо любой сложности</span></div>
    <div class="skidka-block"><span><span class="block-city-date">Киев</span><span class="date-date">31.01.2020</span></span><span class="skidka-spec">-15%</span></div>
  </div>
  
  <div class="carousel-item">
    <img src="/image/catalog/special/img.jpg" alt="Dog" title="Dog">
    <div class="block-name-desc"><span class="name-spec">Видеооператор</span><span class="desc-spec">Сниму ваш праздник и сделаю с него фильм</span></div>
    <div class="skidka-block"><span><span class="block-city-date">Киев</span><span class="date-date">25.01.2020</span></span><span class="skidka-spec">-20%</span></div>
  </div>
  
  <div class="carousel-item">
    <img src="/image/catalog/special/img_2.jpg" alt="Dog" title="Dog">
    <div class="block-name-desc"><span class="name-spec">Кондитер</span><span class="desc-spec">Приготовлю для вас вкусный тортик</span></div>
    <div class="skidka-block"><span><span class="block-city-date">Киев</span><span class="date-date">28.02.2020</span></span><span class="skidka-spec">-45%</span></div>
  </div>
  
</div>


  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="btn-block-all-spec"><a href="/index.php?route=product/special" class="all-special see-vendors">Посмотреть все акции</a></div>
  </div>
</div>

</div>



<script type="text/javascript">


</script>