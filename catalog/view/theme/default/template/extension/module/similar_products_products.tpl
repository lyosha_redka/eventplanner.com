<div class="row product-layout">
	<?php foreach ($products as $product) { ?>
	<?php if ($position == "column_left" || $position == "column_right") {
		$class = 'col-xs-12';
	} else if ($column_left && $column_right) {
		if ($show_in_tab) {
			$class = 'col-xs-12';
		} else {
			$class = 'col-md-6 col-xs-12';
		}
	} elseif ($column_left || $column_right) {
		if ($show_in_tab) {
			$class = 'col-sm-6 col-xs-12';
		} else {
			$class = 'col-md-4 col-sm-6 col-xs-12';
		}
	} else {
		if ($show_in_tab) {
			$class = 'col-md-4 col-sm-6 col-xs-12';
		} else {
			$class = 'col-md-3 col-sm-6 col-xs-12';
		}
	} ?>
	<div class="<?php echo $class ?> sp-p">
		<div class="product-thumb transition">
			<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
			<div class="caption">
				<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
				<p><?php echo $product['description']; ?></p>
				<?php if ($product['rating']) { ?>
				<div class="rating">
					<?php for ($i = 1; $i <= 5; $i++) { ?>
					<?php if ($product['rating'] < $i) { ?>
					<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					<?php } else { ?>
					<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
					<?php } ?>
					<?php } ?>
				</div>
				<?php } ?>
				<?php if ($product['price']) { ?>
				<p class="price">
					<?php if (!$product['special']) { ?>
					<?php echo $product['price']; ?>
					<?php } else { ?>
					<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
					<?php } ?>
					<?php if ($product['tax']) { ?>
					<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
					<?php } ?>
				</p>
				<?php } ?>
			</div>
			<div class="button-group">
				<button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
				<button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
				<button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<?php if ($show_pagination) { ?>
<?php if ($position == "column_left" || $position == "column_right") {
	$class = 'col-xs-12';
} else if ($column_left && $column_right) {
	if ($show_in_tab) {
		$class = 'col-xs-12';
	} else {
		$class = 'col-sm-6';
	}
} else {
	$class = 'col-sm-6';
} ?>
<div class="row">
	<div class="<?php echo $class; ?> text-left sp-nbm"><?php echo $pagination; ?></div>
	<div class="<?php echo $class; ?> text-right"><?php echo $results; ?></div>
</div>
<?php } ?>
