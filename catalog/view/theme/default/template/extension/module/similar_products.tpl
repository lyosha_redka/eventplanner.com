<?php if ($show_similar) { ?>
<?php if (!$show_in_tab) { ?>
<h3><?php echo $heading_title; ?></h3>
<div>
<?php } ?>
	<style scoped type="text/css">.sp-l,.sp-o{background-color:#fff}.sp-c{position:relative;width:100%;overflow:hidden}.sp-o-c{width:100%;height:100%;position:absolute;top:0;left:0;z-index:1000;display:none}.sp-o{width:100%;height:100%;position:absolute;top:0;left:0;color:#ccc;display:-ms-flexbox;display:-webkit-flex;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-flex-wrap:nowrap;-ms-flex-wrap:nowrap;flex-wrap:nowrap;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-content:stretch;-ms-flex-line-pack:stretch;align-content:stretch;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.sp-ul{z-index:999}.sp-ll{z-index:998}.sp-o .fa{font-size:48px}</style>
	<div id="sp-c<?php echo $mid; ?>" class="sp-c"<?php echo $lazy_load ? ' style="height:300px"' : ''; ?>>
		<div id="sp-o<?php echo $mid; ?>" class="sp-o-c">
			<div class="sp-o">
				<i class="fa fa-spin fa-refresh"></i>
			</div>
		</div>
		<div class="sp-ll sp-l" id="sp-p<?php echo $mid; ?>" data-mid="<?php echo $mid; ?>"><?php echo $products; ?></div>
	</div>
<?php if (!$show_in_tab) { ?>
</div>
<?php } ?>
<script type="text/javascript"><!--
!function(p,e,o){p.texts=e.extend({},p.texts,{error_ajax_request:"<?php echo addslashes($error_ajax_request); ?>"})<?php if ($lazy_load) { ?>,e(function(){p.sp_wp_modules.push("<?php echo $mid; ?>"),new Waypoint({element:document.getElementById("sp-p<?php echo $mid; ?>"),handler:function(){var e=p.sp_wp_modules.indexOf("<?php echo $mid; ?>");p.sp_get_products("index.php?route=extension/module/similar_products/get&pid=<?php echo $product_id; ?>&mid=<?php echo $mid; ?>&pos=<?php echo $position; ?><?php echo $path; ?>","<?php echo $mid; ?>"),e>-1&&p.sp_wp_modules.splice(e,1),this.destroy()},offset:"100%"})})<?php } ?>}(window.bull5i=window.bull5i||{},jQuery);
//--></script>
<?php } ?>
