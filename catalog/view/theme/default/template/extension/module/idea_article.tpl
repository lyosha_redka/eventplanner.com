<?php if ($heading_title) { ?>
<h3><?php echo $heading_title; ?></h3>
<?php } ?>
<?php if ($html) { ?>
<?php echo $html; ?>
<?php } ?>
<div class="row">

<div class="col-sm-12">

  <div class="owl-carousel owl-theme">
    

  <?php foreach ($articles as $article) { ?>
  <div class="item">
    <div class="flip-container">
    <div class="idea-article-block">
      <div class="product-article-idea transition post-item-idea front">
        <?php if ($article['thumb']) { ?>
        <div class="image-idea"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a></div>
        <?php } ?>
          <div class="caption-idea">
            <div class="title-article-idea"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
            <div class="article-preview-idea"><?php echo $article['preview']; ?></div>
          </div>
        <!-- <div class="button-group-more idea-group-more">
  		    <button onclick="location.href = ('<?php echo $article['href']; ?>');" data-toggle="tooltip" title="<?php echo $text_more; ?>" class="btn-idea"> <span class="more-text-idea"><?php echo $text_more; ?></span></button>
    	  </div> -->
      </div>

      <div class="back">

        <div class="caption-idea-back">
          <div class="title-article-idea-back"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
          <div class="article-preview-idea-back"><?php echo $article['preview']; ?></div>
        </div>

        <div class="button-group-more idea-group-more">
          <button onclick="location.href = ('<?php echo $article['href']; ?>');" class="btn-idea"> 
            <span class="more-text-idea"><?php echo $text_more; ?></span>
          </button>
        </div>


      </div>
    </div>
  </div>
</div>
  <?php } ?>


</div>

</div>

</div>


<script>
  
  $('.owl-carousel').owlCarousel({
  loop: true,
  margin: 10,
    dot: false,
    nav:true,
    navText: ["<img src='/image/arrow.png' class='imgPrev' />", "<img src='/image/arrow.png' class='imgNext' />"],
    autoplay: false,
    autoplayHoverPause: true,
    responsive: {
      0: {
      items: 1
      },
      600: {
      items: 3
      },
      1000: {
      items: 4
      }
    }
  });

</script>

</div>