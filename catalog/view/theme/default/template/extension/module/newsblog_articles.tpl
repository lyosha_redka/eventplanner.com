<?php if ($heading_title) { ?>
<h3><?php echo $heading_title; ?></h3>
<?php } ?>
<?php if ($html) { ?>
<?php echo $html; ?>
<?php } ?>
<div class="row">
  <?php foreach ($articles as $article) { ?>
  <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="product-thumb transition post-item">
      <?php if ($article['thumb']) { ?>
      <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a></div>
      <?php } ?>
      <div class="caption">
        <div class="title-article"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
        <div class="article-preview"><?php echo $article['preview']; ?></div>
      </div>
      <div class="button-group-more">
		<button onclick="location.href = ('<?php echo $article['href']; ?>');" data-toggle="tooltip" title="<?php echo $text_more; ?>"> <span class="more-text"><?php echo $text_more; ?></span></button>
	  </div>
    </div>
  </div>
  <?php } ?>

  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="/blog">
      <div class="more-posts">
        <span class="see-all-posts">Смотреть все статьи</span>
        <span class="arrow-all-posts"><img src="/image/catalog/arrow.png" class="arrow-right"></span>
      </div>
    </a>
  </div>
</div>