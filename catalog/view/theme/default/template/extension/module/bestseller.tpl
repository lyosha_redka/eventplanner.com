
<div class="section-topy">
  <div class="container">

    <h3 class="title-blog"><?php echo $heading_title; ?></h3>
    <div class="row">
      <div class="col-sm-12">
      <div class="owl-carousel owl-theme best-carousel">
      <?php foreach ($products as $product) { ?>
      
        <div class="item">
          <div class="vertical-flip-container flip-container-top floatR" ontouchstart="this.classList.toggle('hover');">
            <div class="flipper-top">
              

              <div class="front-vert">  
                <div class="product-thumb-topy transition-topy">
                  <div class="image-topy"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  
                </div>
              </div>


              <div class="back-vert">
                <div class="back-flip-desc">
                  <p class="desc-topy-back"><?php echo $product['description']; ?></p>

                  <div class="btn-button-back"><a href="<?php echo $product['href']; ?>">Подробнее</a></div>
                </div>
              </div>


            </div>
          </div>

          <div class="caption-topy">
            <div class="name-topy"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
            <p class="desc-topy"><?php echo $product['description']; ?></p>
          </div>

        </div>
      <?php } ?>
    </div>
    </div>
  </div>

  </div>
</div>

<script type="text/javascript">
  

    $('.owl-carousel.owl-theme.best-carousel').owlCarousel({
  loop: true,
  margin: 10,
    dot: false,
    nav:true,
    navText: ["<img src='/image/arrow.png' class='imgPrev' />", "<img src='/image/arrow.png' class='imgNext' />"],
    autoplay: false,
    autoplayHoverPause: true,
    responsive: {
      0: {
      items: 1
      },
      600: {
      items: 3
      },
      1000: {
      items: 4
      }
    }
  });


</script>