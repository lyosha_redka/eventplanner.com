<?php echo $header; ?>
<div class="container">



  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>


  <div class="row">
        <div class="col-sm-12">
          <div class="img-block-banner">
            <p class="category-banner-vendors">рекламный баннер для топов (рандом)</p>
          </div>
        </div>
      </div>
<div class="top-with-btn">
  <div class="block-top-category">
    <?php echo $content_top; ?>
  </div>
  <div class="krapka">...</div>
  </div>

  <script>
    $(".krapka").click(function(){
      $(".block-top-category").toggleClass("main");
    });
  </script>

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">

      <?php if ($categories) { ?>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>


      
      <div class="category-vendors-block-title">
        <h1 class="title-category"><?php echo $heading_title; ?></h1>
      </div>

      <?php if ($products) { ?>

      <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-grid col-sm-4 col-xs-12">
          <div class="product-thumb">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div>
                          <div class="caption">
              <div class="name-related-vendors"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>


              <?php if ($product['rating']) { ?>

                <div class="rating-block-with-cont">
                  <div class="rating rating-related-vendors">
                    <?php for ($j = 1; $j <= 5; $j++) { ?>
                    <?php if ($product['rating'] < $j) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } ?>
                    <?php } ?>
                  </div>
                  <div class="text-rating-related-products"><?php echo $product['reviews'] . ' отзыв'; ?></div>

                  <div class="btn-to-wishist">
                    <button type="button" data-toggle="tooltip" class="btn btn-default wish-btn-vendors" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><img src="/image/catalog/heart.svg" class="like-img like-img-cat"></button>
                  </div>
                </div>

              <?php } else { ?>
                <div class="rating-block-with-cont">
                  <div class="rating rating-related-vendors">
                    <?php for ($j = 1; $j <= 5; $j++) { ?>
                    <?php if ($product['rating'] < $j) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } ?>
                    <?php } ?>
                  </div>
                  <div class="text-rating-related-products"><?php echo $product['reviews'] . ' отзыв'; ?></div>

                  <div class="btn-to-wishist">
                    <button type="button" data-toggle="tooltip" class="btn btn-default wish-btn-vendors" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><img src="/image/catalog/heart.svg" class="like-img like-img-cat"></button>
                  </div>
                </div>
              <?php } ?>

              <?php if ($product['price']) { ?>
              <p class="price">
                <span class="sred-check"><?php echo 'Средний чек:' ?></span>
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>



      <div class="row">
        <div class="col-sm-12 text-center"><?php echo $pagination; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      </div>
    <?php echo $column_right; ?></div>

      <div class="row">
        <?php if ($description) { ?>
          <div class="col-sm-12">
            <div class="description-category"><?php echo $description; ?></div>
          </div>
        <?php } ?>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="blog-for-category">
            <h2 class="blog-head-category">Блог</h2>
            <?php echo $content_bottom; ?>
          </div>
        </div>
      </div>
</div>
<?php echo $footer; ?>
